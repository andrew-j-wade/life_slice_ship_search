# life_slice_ship_search: A Spaceship Search Program for Conway's Life

## Usage

This is a linux program. To run it under Windows you will need to know how to run unix scripts under windows. You will probably need to also edit search.sh to run the executable with a .exe extension.

1. Install <https://www.rust-lang.org>

2. Clone the repository.

3. Run "RUSTFLAGS='-C target-cpu=native' cargo build --release". The RUSTFLAGS variable is optional.

4. Edit search.sh as desired.

5. Run the search script.

6. Partial results will be saved in the data directory.

## The Algorithm

This algorithm came from a consideration of divide and conquer strategies for finding spaceships.

IF:

1. A left partial spaceship and a right partial spaceship are the same in a 2 column overlap region running through all the generations, and

2. The overlap region never shifts by more than a column sideways from generation to generation,

THEN:

* The combination of the two partial spaceships will be a spaceship.

(Condition 2 is needed so that consistency of each partial spaceship is enough to guarantee the consistency of the result.)

In this algorithm we work solely with the two-column overlap regions, hereafter called slices.

So, for example the spaceship:
```
..***...
..*..*..
..*.....
..*.....
...*.*..
```

Is composed of the following slices:
```
..  .*  **  **  *.  ..  ..
..  .*  *.  ..  .*  *.  ..
..  .*  *.  ..  ..  ..  ..
..  .*  *.  ..  ..  ..  ..
..  ..  .*  *.  .*  *.  ..
```
(Only one generation shown, but these slices would extend through all four generations.)

A slice is part of one or more spaceships if and only if:

1. There exists a slice one column to the left of the slice such that:

    * The slices are the same in the one-column overlap region,
    * The combination of the two slices are consistent with the rules of the cellular automata, and
    * The slice one column to the left is part of one or more spaceships.

2. There exists a slice one column to the right of the slice such that:

    * The slices are the same in the one-column overlap region,
    * The combination of the two slices are consistent with the rules of the cellular automata, and
    * The slice one column to the right is part of one or more spaceships.

There is a circularity to these conditions, but by themselves they are already enough to remove some slices that cannot be part of a spaceship.

To turn these rules into a usable search program we start introducing arbitrary restrictions to reduce the number of slices to a managable number:

* The spaceship is restricted to a certain range of columns.
* The spaceship is restricted to starting after a certain row.

And rather than work with whole slices, the search program works with partial slices (up to a certain row), with rows gradually added as the search progresses.

The advantage of working with partial slices rather than partial spaceships is that a very large set of partial spaceships can be described by a more managable set of partial slices. This is because each partial slice can be part of many partial spaceships.

### An example of the join operation

If we have a partial slice:
```
        **
        *.
        *.
```

And we have a partial slices we could potentially join to of:

```
    .*
    .*
    .*
    .*
```

Then we can produce the following partial slices:
```
        **
        *.
        *.
        *.

        **
        *.
        *.
        **
```

If, when working our way back from the other side of the space we have the following partial slices we could potentially join to of:
```
            **
            ..
            ..
            ..
            *.

            **
            ..
            ..
            ..
            ..
```

Then we can produce the following partial slices:
```
        **
        *.
        *.
        *.
        ..

        **
        *.
        *.
        *.
        *.

        **
        *.
        *.
        *.
        .*

        **
        *.
        *.
        *.
        **
```

Notice in this example that the partial slice:
```
        **
        *.
        *.
        **
```
Was dropped and not extended further. This is because there were not partial slices to the right that it could join to.

The cellular automata rules are also enforced during the join operation.

### Fleshing out the search

The join operation is performed starting from one side of the arena to the other, and then back again:

```
Slice position:
1        2        3        4        5
|        |        |        |        |
|--join->|        |        |        |
|        |        |        |        |
|        |--join->|        |        |
|        |        |        |        |
|        |        |--join->|        |
|        |        |        |        |
|        |        |        |--join->|
|        |        |        |        |
|        |        |        |<-join -|
|        |        |        |        |
|        |        |<-join--|        |
|        |        |        |        |
|        |<-join--|        |        |
|        |        |        |        |
|<-join--|        |        |        |
|        |        |        |        |
...
```

Each pass from one side of the space to the other another row is added to the partial slices. The slices so produced will not necessarily form part of a partial spaceship, but those that do not will only be reachable from one side of the arena through the join operation, and will thus be dropped in the next pass.

A row here consists of a row+generation combination. This search program uses a transformation very much like the one described in [arXiv:cs/0004003](https://arxiv.org/abs/cs/0004003).

**Q:** Can we slice in additional directions?

**A:** Yes, but...

When a spaceship is sliced in multiple directions you lose the property of being able to guarantee a global solution from the existance of local joins. The difficulty is the presence of cycles when joining the fragments; there may not be fragments with the right joins to close the cycles, and this lack is not something that can be discovered with local analysis. For similar reasons there is no longer a simple procedure for extracting a partial spaceship.

That said, the ahead2 bit masks in life_slice_ship_search could be thought of slicing across the time direction at the very tail of the slices. And indeed, partial spaceship extraction sometimes fails part way through, likely as a result of cycles in the ahead2 relationships. There would likely be some benefit for slicing in the time direction at the tail more so than life_slice_ship_search does, but the slices should probably remain full height (all generations) except quite near the tail.

### Extracting a partial spaceship

A partial spaceship is extracted by a modification of the join operation that somewhat arbitrarily picks one partial slice for each slice position that joins to the partial slice picked for the previous slice position.

### Extracting a full spaceship

Additional metadata is added to the slices (minimum distance to a ship edge at the tail) that is used to drive the choice of slice to add with the result that if full spaceships exist one will be chosen.

### Representation of the slices

The set of partial slices for a given slice position is stored in memory as a tree with deeper nodes corresponding to rows closer to the tail of the partial slice. The nodes are annotated with lookahead information that can be used to exclude whole branches of trees for adjacent slice positions from being joined to each other.

As the number of partial slices can be very large it is not practical to store the whole set in RAM. Branches ("chunks") of the tree are loaded from disk as needed, and branches are written back out to disk as they are generated. When writing the chunks back out they are first buffered up and sorted in the order they be needed for the next join operation before being written back out. Both the read side and the write site use sequential io patterns suitable for rotating drives. The read side can potentially be reading a large number of files at the same time (it's essentially doing a merge sort) and relies on os-level readahead to generate reasonable io patterns.

## On disk structure

The partial slices are stored in files with a separate directory for each slice position, and higher-level directories for how many rows in each partial slice. The index files contain the chunk headers, which have a tail-compressed representation of the cells toward the head of the tree. The "branches" files contain the serialized trees, with optional arithmetic coding.

## See also

conwaylife.com [discussion thread](https://www.conwaylife.com/forums/viewtopic.php?f=9&t=3413).

[Searching for Spaceships](https://arxiv.org/pdf/cs/0004003.pdf) by David Eppstein. This paper has a quite different search algorithm, but some of the ideas inspired the approach here, particularly the discussion of de Bruijn graphs.