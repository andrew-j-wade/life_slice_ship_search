#![allow(clippy::inconsistent_digit_grouping)]
#![allow(clippy::verbose_bit_mask)]
#![allow(clippy::identity_op)]

use crate::chunkio::Compression;
use crate::chunkio::SliceDesc;

use crate::compress::ArithmeticCompressor;
use crate::compress::ArithmeticDecompressor;
use crate::compress::Compress;
use crate::compress::Decompress;
use crate::compress::PackedCompressor;
use crate::compress::PackedDecompressor;
use crate::compress::RleCompressor;
use crate::compress::RleDecompressor;

use std::collections::HashMap;
use std::default::Default;

use std_semaphore::{Semaphore, SemaphoreGuard};

use std::mem::replace;

use std::cmp::{max, min};

use std::convert::{From, TryFrom, TryInto};

use regex::Regex;
use std::error::Error;

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct Position {
    pub gen: i32,
    pub y: i32,
}

pub fn parse_starts(starts: &str) -> Result<Box<[Position]>, String> {
    let mut rv: Vec<Position> = Vec::new();

    let mut starts_str: Vec<&str> = starts.split(',').collect();

    while !starts_str.is_empty() {
        rv.push(Position {
            gen: starts_str[0]
                .parse::<i32>()
                .map_err(|err| format!("Invalid gen in --starts: {}", err.to_string()))?,
            y: starts_str[1]
                .parse::<i32>()
                .map_err(|err| format!("Invalid y in --starts: {}", err.to_string()))?,
        });
        starts_str.remove(0);
        starts_str.remove(0);
    }
    Ok(rv.into_boxed_slice())
}

pub fn parse_shifts(shifts: &str) -> Result<Box<[i32]>, String> {
    let mut rv: Vec<i32> = Vec::new();

    if shifts.is_empty() {
        return Ok(rv.into_boxed_slice());
    };

    for shift in shifts.split(',') {
        rv.push(
            shift
                .parse::<i32>()
                .map_err(|err| format!("Invalid --shift value: {}", err.to_string()))?,
        );
    }

    Ok(rv.into_boxed_slice())
}

pub fn parse_velocity(
    velocity: &str,
) -> Result<(usize, Box<[Position]>, Box<[i32]>), Box<dyn Error + Send + Sync>> {
    let re = Regex::new(r"^\((\d+),(\d+)\)c/(\d+)$").unwrap();
    let caps = re.captures(velocity).ok_or("wrong format for velocity")?;
    let k: i32 = caps.get(1).unwrap().as_str().parse()?;
    let b: i32 = caps.get(2).unwrap().as_str().parse()?;
    let p: i32 = caps.get(3).unwrap().as_str().parse()?;

    if k <= 0 || k >= p {
        return Err("Vertical velocity must be strictly between 0 and c".into());
    };
    if b < 0 || b > p {
        return Err("Horizonal velocity must be nonnegative and not greater than c.".into());
    }

    let mut candidates: Vec<_> = (0..p)
        .flat_map(|g| (0..(p - k)).map(move |y| Position { gen: g, y }))
        .filter(|pos| pos.gen * k + pos.y * p >= (k - 1) * p)
        .collect();

    // depends on a stable sort to give consistent results when gcd(k,p) > 1
    candidates.sort_by(|a, b| (a.gen * k + a.y * p).cmp(&(b.gen * k + b.y * p)));
    candidates.truncate((p - k).try_into().unwrap());

    let shifts: Vec<_> = (0..b).map(|shift| (shift * p) / b).collect();

    Ok((
        p.try_into().unwrap(),
        candidates.into_boxed_slice(),
        shifts.into_boxed_slice(),
    ))
}

#[derive(Clone, Copy, Debug, Default)]
pub struct LvlLookBack {
    middlerow_future: usize,
    middlerow: usize,
    headrow: usize,
    shift: bool,
    prevshift: bool,
    gen: i32,
}

impl LvlLookBack {
    pub fn prepare(
        starts: &[Position],
        shifts: &[i32],
        lvls: usize,
        period: usize,
    ) -> Box<[LvlLookBack]> {
        let mut look_backs = Vec::<LvlLookBack>::new();
        let mut pos_indexes: HashMap<Position, usize> = HashMap::new();
        let aheadrows = starts.len();
        let mut starts = Box::<[Position]>::from(starts);
        // level zero is the root node that doesn't correspond to a
        // spaceship search row
        look_backs.push(LvlLookBack {
            middlerow_future: 1 << 31,
            headrow: 0,
            middlerow: 0,
            shift: false,
            prevshift: false,
            gen: 0,
        });
        for i in 0..lvls {
            let mut start = starts[i % aheadrows];
            let gen = start.gen;
            pos_indexes.insert(start, i + 1);
            start.y += 1;
            start.gen -= 1;
            if start.gen < 0 {
                start.gen += period as i32;
                start.y += aheadrows as i32 - period as i32;
            };
            starts[i % aheadrows] = start;
            let lookback = LvlLookBack {
                middlerow_future: 1 << 31,
                headrow: *pos_indexes
                    .get(&Position {
                        y: start.y - 2,
                        ..start
                    })
                    .unwrap_or(&0),
                middlerow: *pos_indexes
                    .get(&Position {
                        y: start.y - 1,
                        ..start
                    })
                    .unwrap_or(&0),
                shift: shifts.contains(&gen),
                prevshift: shifts.contains(&start.gen),
                gen,
            };
            look_backs[lookback.middlerow].middlerow_future = i + 1;
            look_backs.push(lookback);
        }

        look_backs.into_boxed_slice()
    }
}

pub struct Lookups {
    self_to_other: [u64; 6 << 2],
    cells: [u8; 1 << 6],
    cells_shift: [u8; 1 << 6],
    prev_cells: u64,
    prev_cells_shift: u64,
}

impl Lookups {
    pub fn create(rule: &str) -> (Lookups, Lookups) {
        let mut birthcounts = 0u32;
        let mut survivalcounts = 0u32;

        let rule_str: Vec<&str> = rule.split('/').collect();

        for c in rule_str[0].chars() {
            if let Some(m) = c.to_digit(10) {
                birthcounts |= 1 << m;
            };
        }
        for c in rule_str[1].chars() {
            if let Some(m) = c.to_digit(10) {
                survivalcounts |= 1 << m;
            };
        }

        let mut left_to_right = [0u64; 6 << 2];
        let mut right_to_left = [0u64; 6 << 2];
        let mut lkup_cells_left = [0u8; 1 << 6];
        let mut lkup_cells_left_shift = [0u8; 1 << 6];
        let mut lkup_cells_right = [0u8; 1 << 6];
        let mut lkup_cells_right_shift = [0u8; 1 << 6];

        fn hits(lkup: usize, looked_up: usize) -> u64 {
            let mut rv = 0u64;

            for i in 0..16 {
                rv |= 1u64 << ((i | (1 << lkup)) * 4 + looked_up);
            }
            rv
        }

        for neighbors in 0..6usize {
            for left in 0..4usize {
                for right in 0..4usize {
                    if ((left & 1) != 0) != ((right & 2) != 0) {
                        continue;
                    };

                    let neighborcount = neighbors + ((left << 1) | right).count_ones() as usize;

                    if (birthcounts & (1 << neighborcount)) != 0 {
                        left_to_right[(neighbors << 2) + 0b01] |= hits(left, right);
                        right_to_left[(neighbors << 2) + 0b01] |= hits(right, left);
                    } else {
                        left_to_right[(neighbors << 2) + 0b00] |= hits(left, right);
                        right_to_left[(neighbors << 2) + 0b00] |= hits(right, left);
                    };

                    if (survivalcounts & (1 << neighborcount)) != 0 {
                        left_to_right[(neighbors << 2) + 0b11] |= hits(left, right);
                        right_to_left[(neighbors << 2) + 0b11] |= hits(right, left);
                    } else {
                        left_to_right[(neighbors << 2) + 0b10] |= hits(left, right);
                        right_to_left[(neighbors << 2) + 0b10] |= hits(right, left);
                    };
                }
            }
        }

        // Iterate over all 4x3 neighborhoods and update the lookups
        // for each one.

        for cells in 0..0b1_0000_0000_0000usize {
            let mut lkup = ((cells & 0b0110_0000_0000) >> 7) + ((cells & 0b0000_0110_0000) >> 5);
            let mut lkup_shift = lkup;

            let counts = if cells & 0b0000_0100_0000 != 0 {
                survivalcounts
            } else {
                birthcounts
            };
            if counts & (1 << (cells & 0b1110_1010_1110).count_ones()) != 0 {
                lkup |= 0b10_00_00;
            };
            let counts = if cells & 0b0000_0010_0000 != 0 {
                survivalcounts
            } else {
                birthcounts
            };
            if counts & (1 << (cells & 0b0111_0101_0111).count_ones()) != 0 {
                lkup |= 0b01_00_00;
                lkup_shift |= 0b10_00_00;
            };

            let ahead_mask = 1u8 << ((cells & 0b0110) >> 1);
            lkup_cells_left[lkup] |= ahead_mask;
            lkup_cells_right[lkup] |= ahead_mask;
            lkup_cells_left_shift[lkup_shift] |= ahead_mask;
            lkup_cells_right_shift[lkup_shift] |= ahead_mask;
            lkup_cells_left_shift[lkup_shift | 0b01_00_00] |= ahead_mask;
            lkup_cells_right_shift[lkup_shift | 0b01_00_00] |= ahead_mask;
        }

        // update the cells
        for cells in 0..0b1_00_00_00usize {
            lkup_cells_left[cells] |= if (cells & 0b01) != 0 { 0b0010_0000 } else { 0 }
                + if (cells & 0b01_00_00) != 0 {
                    0b0001_0000
                } else {
                    0
                }
                + (((cells & 0b00_11_10).count_ones() as u8) << 6);
            lkup_cells_left_shift[cells] |= if (cells & 0b01) != 0 { 0b0010_0000 } else { 0 }
                + if (cells & 0b10_00_00) != 0 {
                    0b0001_0000
                } else {
                    0
                }
                + (((cells & 0b00_11_10).count_ones() as u8) << 6);
            lkup_cells_right[cells] |= ((cells & 0b00_01_01).count_ones() as u8) << 6;
            lkup_cells_right_shift[cells] |= ((cells & 0b00_01_01).count_ones() as u8) << 6;
        }

        let mut prev_cells_left = 0u64;
        let mut prev_cells_right = 0u64;
        let mut prev_cells_left_shift = 0u64;
        let mut prev_cells_right_shift = 0u64;

        for b45 in 0..4 {
            for b23 in 0..4 {
                let lkup = (b45 << 4) + b23;
                let shift = (b45 << 4) + (b23 << 2);
                prev_cells_left |= ((lkup_cells_left[lkup] >> 4) as u64) << shift;
                prev_cells_right |= ((lkup_cells_right[lkup] >> 4) as u64) << shift;
                prev_cells_left_shift |= ((lkup_cells_left_shift[lkup] >> 4) as u64) << shift;
                prev_cells_right_shift |= ((lkup_cells_right_shift[lkup] >> 4) as u64) << shift;
            }
        }

        (
            Lookups {
                self_to_other: left_to_right,
                cells: lkup_cells_left,
                cells_shift: lkup_cells_left_shift,
                prev_cells: prev_cells_left,
                prev_cells_shift: prev_cells_left_shift,
            },
            Lookups {
                self_to_other: right_to_left,
                cells: lkup_cells_right,
                cells_shift: lkup_cells_right_shift,
                prev_cells: prev_cells_right,
                prev_cells_shift: prev_cells_right_shift,
            },
        )
    }
}

#[derive(Clone, Copy, Debug, Default)]
struct TreeNode {
    child: u8,
    ahead: u8,
    // bit:
    // 0-3 - allowed children/lookahead rows.
    //       i.e. if bit 0 is set, bb is a child/lookahead row
    //            if bit 1 is set, bo is a child/lookahead row
    //            if bit 2 is set, ob is a child/lookahead row
    //            if bit 3 is set, oo is a child/lookahead row
    // 4   - current gen cell
    // 5   - prev gen mid cell
    // 6-7 - count of surrounding active cells in prev gen

    // For the right slice, bits 4-7 are set only depending on the
    // rightmost column of cells. In this way adding bits 4-7 from
    // the left and from the right slice gives the state of the neighborhood.
    // (The count of surrounding cells can overflow, but the join
    // algorithm accounts for this by shifting the bits down first.)
    prev: u8,
    // bit:
    // 0 chunkhead flag
    // 1 all_zeros flag
    // 2-3 number of cells ahead and in the previous^2 generation.
    // 4 prev gen mid cell
    // 5 prev gen^2 mid cell
    // 6-7 count of surrounding active cells in prev gen^2
    dist: u8, // distance to the spaceship edge.

    //Save space by using only 32 bit indexes for the tree indexes.
    //We can limit tree size by adjusting chunk sizes, so the 32 bit limitation
    //shouldn't be a problem.
    children: u32,

    // cells in a triangular lookahead region.
    ahead2: u64,
    //
    // a  ^ future
    // bc  > tail
    // bits:
    // 45 - a
    // 23 - b
    // 01 - c
}

impl TreeNode {
    fn chunkhead(&self) -> bool {
        self.prev & 1 != 0
    }
    fn setchunkhead(&mut self) {
        self.prev |= 1;
    }
    fn all_zeros(&self) -> bool {
        self.prev & 2 != 0
    }
    fn set_all_zeros(&mut self) {
        self.prev |= 2;
    }
    fn children(&self) -> usize {
        self.children as usize
    }
    fn set_children(&mut self, children: usize) {
        // We should be able to avoid overflowing u32 for the number of nodes,
        // but if not panic to avoid corrupting the tree.
        self.children = children.try_into().unwrap();
    }
    fn new(child: u8, ahead: u8, ahead2: u64, chunkhead: bool) -> TreeNode {
        TreeNode {
            child,
            ahead,
            prev: if chunkhead { 1 } else { 0 },
            dist: 0,
            children: Default::default(),
            ahead2,
        }
    }
}

#[derive(Default, Debug)]
struct Level {
    matches: Vec<u32>,
    tp: usize,
    child: u8,
    node: TreeNode,
    new_ahead_mask: u8,
    new_ahead_mask2: u8,
}

pub struct Workspace<'a> {
    lvl: Vec<Level>,
    node: Vec<TreeNode>,
    key_cache: [Vec<u8>; 2],
    pub large_threshold: [isize; 4],
    // Threads can use up to large_threshold[0] memory for free
    // The number of running threads using more is restricted to
    // avoid using too much memory.
    pub large_sem: &'a [Semaphore; 4],
    pub large_guard: [Option<SemaphoreGuard<'a>>; 4],
}

impl<'a> Workspace<'a> {
    pub fn new(levels: usize, large_sem: &'a [Semaphore; 4]) -> Workspace<'a> {
        Workspace {
            lvl: (0..(levels + 1)).map(|_| Level::default()).collect(),
            node: Vec::new(),
            key_cache: [Vec::new(), Vec::new()],
            large_threshold: [isize::max_value(); 4],
            large_sem,
            large_guard: [None, None, None, None],
        }
    }

    pub fn clear(&mut self) {
        for lvl in &mut self.lvl {
            lvl.matches.clear();
        }
        self.node.clear();

        if self.large_guard[0].is_some() {
            //Free memory then release the semaphores
            for lvl in self.lvl.iter_mut() {
                *lvl = Level::default();
            }
            self.node = Vec::new();
            for guard in self.large_guard.iter_mut() {
                *guard = None;
            }
        }
    }

    pub fn size(&self) -> usize {
        self.node.len() * 16
    }

    fn _seed_tree(
        &mut self,
        seed: &[char],
        root: usize,
        row: usize,
        tree_rows: usize,
        ahead_rows: usize,
    ) {
        self.node[root].child = seed[0].to_digit(16).unwrap() as u8;
        self.node[root].ahead = seed[min(seed.len() - 1, ahead_rows - 1)]
            .to_digit(16)
            .unwrap() as u8;

        let mut ahead2 = seed[min(seed.len() - 1, ahead_rows * 2 - 1)]
            .to_digit(16)
            .unwrap() as u64;
        ahead2 += ahead2 << 4;
        ahead2 += ahead2 << 8;
        ahead2 += ahead2 << 16;
        ahead2 += ahead2 << 32;
        self.node[root].ahead2 = ahead2;

        if row == tree_rows {
            return;
        };

        let mut tp = self.node.len();
        let childseed = if seed.len() > 1 { &seed[1..] } else { seed };

        self.node[root].set_children(tp);
        for i in 0..4 {
            if (self.node[root].child & (1u8 << i)) != 0 {
                self.node.push(TreeNode::default());
            };
        }
        for i in 0..4 {
            if (self.node[root].child & (1u8 << i)) != 0 {
                self._seed_tree(childseed, tp, row + 1, tree_rows, ahead_rows);
                tp += 1;
            };
        }
    }

    pub fn seed_tree(&mut self, seed: &[char], tree_rows: usize, ahead_rows: usize) -> usize {
        let root = self.node.len();

        self.node.push(TreeNode::default());
        self._seed_tree(&seed, root, 0, tree_rows, ahead_rows);

        // because the root node is a standin for all rows before the spaceship
        // we want all ahead combinations set.
        self.node[root].ahead = 0x0f;
        self.node[root].ahead2 = u64::max_value();

        root
    }

    // Calculates bits 4-7 of child and ahead, and 0-3 of ahead, along with
    // ahead2 and dist.
    // Also updates the allzeros flag.
    // For dist it applies the restriction that dist will never be less than
    // the minimum dist of the children; this allows bailing from some joins
    // that will not work closer to the root of the tree.
    //
    // The 0-3 calculation has some limitations:
    // * It does not bubble empty masks up to parent nodes if all children
    //   have empty masks,
    // * It is based solely on nodes with non-zero masks so could set bits
    //   that have already been cleared.
    //
    // Do not call after join as this will add back branches that join()
    // has removed from the tree.
    //
    // This function will also add levels to the tree so that
    // the join function is joining trees of the same depth.

    // In this function lvl[sp].node.ahead2 and lvl[sp].node.dist
    // do not represent the current values of these nodes, but the
    // values the nodes should be updated with when walking back to the root
    // of the tree.

    pub fn calc_cells(
        &mut self,
        root: usize,
        tree_rows: usize,
        populated_tree_rows: usize,
        ahead2_mask: u64,
        ahead_rows: usize,
        lookbacks: &[LvlLookBack],
        lookups: &Lookups,
    ) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let lkup = &lookups.cells;
        let shift_lkup = &lookups.cells_shift;

        lvl[0].tp = root;
        // level 0 doesn't correspond to a spaceship row.
        // lookbacks will refer to level 0 for rows ahead of the spaceship
        // so it is important for child[0] to be 0 (all cells empty.)
        lvl[0].child = 0;
        lvl[0].node = node[root];

        let mut tp = lvl[0].node.children();
        let mut child = lvl[0].node.child.trailing_zeros() as u8;
        let mut sp = 1usize;

        while sp > 0 {
            if sp > populated_tree_rows {
                let child_mask;
                let ahead_mask;
                if sp >= ahead_rows {
                    ahead_mask = (node[lvl[sp - ahead_rows].tp].ahead2
                        >> ((child << 4) + (lvl[lookbacks[sp].middlerow].child << 2)))
                        as u8;
                    if ahead_rows == 1 {
                        child_mask = ahead_mask;
                    } else {
                        child_mask = lvl[sp - ahead_rows + 1].node.ahead;
                    };
                } else {
                    ahead_mask = 0x0f;
                    child_mask = 0x0f;
                };

                node.push(TreeNode::new(child_mask, ahead_mask, ahead2_mask, false));
                node[tp].dist = node[lvl[sp - 1].tp].dist;
            };
            let cells_lkup = ((child << 4)
                + (lvl[lookbacks[sp].headrow].child << 2)
                + (lvl[lookbacks[sp].middlerow].child << 0))
                & 0x3f;
            let cells_ahead = if lookbacks[sp].shift {
                shift_lkup[cells_lkup as usize]
            } else {
                lkup[cells_lkup as usize]
            };
            node[tp].ahead |= 0xf0;
            node[tp].ahead &= cells_ahead;
            if ahead_rows == 1 {
                node[tp].child &= 0x0f;
                node[tp].child |= cells_ahead & 0xf0;
            } else if sp >= ahead_rows {
                node[tp].child &= 0x0f;
                node[tp].child |= lvl[sp + 1 - ahead_rows].node.ahead & 0xf0;
            };
            node[tp].prev &= 0x01;
            node[tp].prev |= node[lvl[lookbacks[sp].middlerow].tp].ahead & 0xf0;
            node[tp].prev |=
                lkup[(lvl[lookbacks[lookbacks[sp].middlerow].middlerow].child << 2) as usize] >> 4;

            if sp < tree_rows {
                if sp >= populated_tree_rows {
                    let newtp = node.len();
                    node[tp].set_children(newtp);
                };
                let newtp = node[tp].children();
                lvl[sp].node = node[tp];
                lvl[sp].node.ahead2 = 0;
                lvl[sp].node.dist = 127;
                lvl[sp].tp = tp;
                lvl[sp].child = child;
                tp = newtp;
                child = lvl[sp].node.child.trailing_zeros() as u8;
                lvl[sp].new_ahead_mask = 0;
                sp += 1;
                if child < 4 {
                    continue;
                } else {
                    child = 4;
                };
            };

            while sp > 0 {
                if child < 4 {
                    if sp + ahead_rows <= tree_rows {
                        node[tp].ahead &= 0xf0;
                        node[tp].ahead += lvl[sp].new_ahead_mask;
                        node[tp].ahead2 = lvl[sp].node.ahead2;
                        for i in 0..4 {
                            if node.len() as isize * 16 > self.large_threshold[i] {
                                if self.large_guard[i].is_none() {
                                    self.large_guard[i] = Some(self.large_sem[i].access());
                                };
                            } else {
                                break;
                            };
                        }
                    };
                    if sp >= ahead_rows && node[tp].ahead & 0x0f != 0 {
                        lvl[sp - ahead_rows].new_ahead_mask |= 1 << child;
                        lvl[sp - ahead_rows].node.ahead2 |= ((node[tp].ahead & 0x0f) as u64)
                            << ((child << 4) + (lvl[lookbacks[sp].middlerow].child << 2))
                    };
                    if sp < tree_rows {
                        node[tp].dist = max(node[tp].dist, lvl[sp].node.dist);
                    };
                    lvl[sp - 1].node.dist = min(lvl[sp - 1].node.dist, node[tp].dist);

                    if child == 0
                        && lvl[lookbacks[sp].middlerow].child == 0
                        && node[tp].ahead & 1 != 0
                        && node[tp].ahead2 & 1 != 0
                        && node[lvl[lookbacks[sp].middlerow].tp].ahead & 1 != 0
                    {
                        node[tp].set_all_zeros();
                    };

                    tp += 1;
                    child += 1;
                    child += (lvl[sp - 1].node.child >> child).trailing_zeros() as u8;
                    if child < 4 {
                        break;
                    };
                };
                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
            }
        }
    }

    fn odd_ahead_mask(child: u8, headchild: u8, middlechild: u8, left_to_right: &[u64]) -> u8 {
        let mut cells_ahead = 0;

        // We don't need to worry about shift rows,
        // there will not be any for symmetrical searches.
        if child >= 2 {
            cells_ahead = 0b0001;
        };
        if headchild >= 2 {
            cells_ahead += 0b0100;
        };
        if headchild & 1 != 0 {
            cells_ahead += 0b0100 * 2;
        };
        if middlechild >= 2 {
            cells_ahead += 0b0010;
        };
        if middlechild & 1 != 0 {
            cells_ahead += 0b0100 * 2;
        };

        let mut ahead_mask = 0;

        ahead_mask += (left_to_right[cells_ahead] >> 0b0001_00) as u8 & 0b0001;
        ahead_mask += (left_to_right[cells_ahead] >> 0b0100_00) as u8 & 0b0010;
        ahead_mask += (left_to_right[cells_ahead] >> 0b0010_00) as u8 & 0b0100;
        ahead_mask += (left_to_right[cells_ahead] >> 0b1000_00) as u8 & 0b1000;

        ahead_mask
    }

    // Calculate allowed ahead masks where the left column is the midline.
    // Uses the existing ahead masks and applies further symmetry requirements.
    pub fn calc_odd_midline_ahead(
        &mut self,
        root: usize,
        tree_rows: usize,
        ahead_rows: usize,
        lookbacks: &[LvlLookBack],
        lookups: &Lookups,
    ) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let mut tp = node[root].children();
        let mut child = 0u8;
        let mut sp = 1usize;
        let left_to_right = &lookups.self_to_other;

        lvl[0].tp = root;
        // level 0 doesn't correspond to a spaceship row.
        // lookbacks will refer to level 0 for rows ahead of the spaceship
        // so it is important for child[0] to be 0 (all cells empty.)
        lvl[0].child = 0;
        lvl[0].node = node[root];

        while sp > 0 {
            if lvl[sp - 1].node.child & (1 << child) != 0 {
                node[tp].ahead &= 0xf0
                    + Workspace::odd_ahead_mask(
                        child,
                        lvl[lookbacks[sp].headrow].child,
                        lvl[lookbacks[sp].middlerow].child,
                        left_to_right,
                    );

                // for the first few rows we need to check they are valid
                // for the start of the spaceship.
                if sp <= ahead_rows
                    && Workspace::odd_ahead_mask(0, 0, 0, left_to_right) & (1 << child) == 0
                {
                    node[tp].ahead &= 0xf0;
                };

                let mut ahead2_mask = 0u64;
                for b45 in 0..4 {
                    for b23 in 0..4 {
                        ahead2_mask |= (Workspace::odd_ahead_mask(
                            b45,
                            lvl[lookbacks[lookbacks[sp].middlerow].middlerow].child,
                            b23,
                            left_to_right,
                        ) as u64)
                            << ((b45 << 4) + (b23 << 2));
                    }
                }
                node[tp].ahead2 &= ahead2_mask;

                if sp < tree_rows {
                    let newtp = node[tp].children();
                    lvl[sp].node = node[tp];
                    lvl[sp].tp = tp;
                    lvl[sp].child = child;
                    tp = newtp;
                    child = 0;
                    sp += 1;
                    continue;
                };
            };

            while sp > 0 {
                if lvl[sp - 1].node.child & (1 << child) != 0 {
                    tp += 1;
                };
                if child < 3 {
                    child += 1;
                    break;
                };
                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
            }
        }
    }

    fn join_ahead(
        node: &TreeNode,
        other: &TreeNode,
        middle_ahead: u8,
        prev_cells: u64,
        prev_cells_other: u64,
        self_to_other: &[u64],
        other_to_self: &[u64],
    ) -> u64 {
        let mut rv = 0u64;

        let prev_other = self_to_other[((node.prev >> 4) + (other.prev >> 4)) as usize];

        for b45 in 0..4 {
            if node.ahead & (1 << b45) == 0 {
                continue;
            };
            let o45mask = (self_to_other[((node.ahead >> 4) + (other.ahead >> 4)) as usize]
                >> ((1 << b45) << 2)) as u8
                & other.ahead
                & 0x0f;
            if o45mask == 0 {
                continue;
            };

            let mut b23 = middle_ahead.trailing_zeros();
            while b23 < 4 {
                let ahead2_nibble = (node.ahead2 >> ((b45 << 4) + (b23 << 2))) & 0xf;
                if ahead2_nibble == 0 {
                    b23 += 1;
                    b23 += (middle_ahead >> b23).trailing_zeros();
                    continue;
                };

                let cells01 = ((prev_cells >> ((b45 << 4) + (b23 << 2))) as u8 & 0xf)
                    + (node.prev & 0b1100)
                    + (other.prev & 0b1100);

                let o23mask = (prev_other >> ((1 << b23) << 2)) as u8 & 0x0f;

                let mut ahead_mask = 0;

                let mut o45 = o45mask.trailing_zeros();

                while o45 < 4 {
                    let mut o23 = o23mask.trailing_zeros();
                    while o23 < 4 {
                        let other_shift = (o45 << 4) + (o23 << 2);

                        // This is a hotspot. Profiling suggested
                        // the bounds checks were taking 4% cpu.
                        unsafe {
                            let cells = cells01 + ((prev_cells_other >> other_shift) as u8 & 0x0f);

                            ahead_mask |= other_to_self.get_unchecked(cells as usize)
                                >> (((other.ahead2 >> other_shift) & 0x0f) << 2);
                        };
                        o23 += 1;
                        o23 += (o23mask >> o23).trailing_zeros();
                    }
                    o45 += 1;
                    o45 += (o45mask >> o45).trailing_zeros();
                }

                rv |= (ahead_mask & 0xf) << ((b45 << 4) + (b23 << 2));

                b23 += 1;
                b23 += (middle_ahead >> b23).trailing_zeros();
            }
        }
        rv & node.ahead2
    }

    //Calculates ahead masks by joining to consistent slices.
    //
    //The function will also bubble empty ahead masks up to parent nodes.

    pub fn join(
        &mut self,
        root: usize,
        other_tree: usize,
        tree_rows: usize,
        ahead_rows: usize,
        max_dist: u8,
        update_dist: bool,
        lookbacks: &[LvlLookBack],
        lkup_self: &Lookups,
        lkup_other: &Lookups,
    ) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;

        let self_to_other = &lkup_self.self_to_other;
        let other_to_self = &lkup_other.self_to_other;

        lvl[0].tp = root;
        lvl[0].child = 0;
        lvl[0].node = node[root];
        lvl[0].matches.clear();
        lvl[0].matches.push(other_tree.try_into().unwrap());

        let mut tp = node[root].children();
        let mut child = lvl[0].node.child.trailing_zeros() as u8;
        let mut sp = 1usize;

        while sp > 0 {
            if sp < ahead_rows || lvl[sp - ahead_rows].node.ahead & (1 << child) != 0 {
                let mut new_node = node[tp];
                let ahead_mask = new_node.ahead & 0x0f;

                if ahead_mask == 0 {
                    // pass
                } else {
                    let mut new_dist = 126; //avoid overflow when adding

                    let mut new_ahead2 = 0u64;
                    let mut matches = replace(&mut lvl[sp].matches, Vec::new());
                    let cells_children_init = lvl[sp - 1].node.child >> 4;
                    matches.clear();

                    let join_shift = (1 << child) << 2;

                    if sp + ahead_rows >= tree_rows && new_node.dist > max_dist {
                        new_node.ahead2 &= 1;
                    };

                    let prev_cells = if lookbacks[sp].prevshift {
                        lkup_self.prev_cells_shift
                    } else {
                        lkup_self.prev_cells
                    };
                    let prev_cells_other = if lookbacks[sp].prevshift {
                        lkup_other.prev_cells_shift
                    } else {
                        lkup_other.prev_cells
                    };

                    for &parent in &lvl[sp - 1].matches {
                        let parent = parent as usize;
                        let o_child_mask = node[parent].child;
                        let cells_children = cells_children_init + (o_child_mask >> 4);
                        let join_children =
                            (self_to_other[cells_children as usize] >> join_shift) as u8 & 0b1111;
                        if o_child_mask & join_children == 0 {
                            continue;
                        };

                        let mut op = node[parent].children as usize;
                        let mut ochild = o_child_mask.trailing_zeros();
                        while ochild < 4 {
                            if (1 << ochild) & join_children != 0 {
                                let mut join_ahead2_mask = 0;
                                if new_node.dist + node[op].dist <= max_dist
                                    || new_node.dist == 0
                                    || node[op].dist == 0
                                {
                                    join_ahead2_mask = Workspace::join_ahead(
                                        &new_node,
                                        &node[op],
                                        node[lvl[lookbacks[sp].middlerow].tp].ahead,
                                        prev_cells,
                                        prev_cells_other,
                                        self_to_other,
                                        other_to_self,
                                    );
                                };

                                if sp + ahead_rows > tree_rows {
                                    // various floating row boundary special cases
                                    if node[op].dist + 1 >= max_dist {
                                        // We don't need to set join_ahead2_mask
                                        // here to support floating rows, but it
                                        // will help lookahead be more effective.
                                        if node[op].dist >= max_dist {
                                            join_ahead2_mask &= 1;
                                        } else {
                                            join_ahead2_mask &= 0x0000_0505_0033_0537;
                                        }
                                    };

                                    if node[op].dist == 0 {
                                        if new_dist != 0
                                            && join_ahead2_mask & 1 != 0
                                            && new_node.all_zeros()
                                            && node[op].ahead2 & 1 != 0
                                            // check that the aheads actually join to each other
                                            && self_to_other[
                                                ((new_node.ahead >> 4) + (node[op].ahead >> 4)) as usize
                                                ] & 0x10 != 0
                                            && self_to_other[
                                                ((new_node.prev >> 4) + (node[op].prev >> 4)) as usize
                                                ] & 0x10 != 0
                                            && self_to_other[
                                                ((new_node.prev & 0b1100) + (node[op].prev & 0b1100)) as usize
                                                ] & 0x10 != 0
                                        {
                                            new_dist = 0;
                                        } else if join_ahead2_mask & 0x0000_0505_0033_0537 != 0 {
                                            new_dist = min(new_dist, 1);
                                        } else if join_ahead2_mask != 0 {
                                            new_dist = min(new_dist, 2);
                                        };
                                    } else if join_ahead2_mask != 0 {
                                        new_dist = min(new_dist, node[op].dist + 1);
                                    };
                                };
                                if join_ahead2_mask != 0 {
                                    new_ahead2 |= join_ahead2_mask;
                                    matches.push(op.try_into().unwrap());
                                };
                            };
                            op += 1;
                            ochild += 1;
                            ochild += (o_child_mask >> ochild).trailing_zeros();
                        }
                    }

                    let mut new_ahead = new_node.ahead & 0xf0;
                    if new_ahead2 & (0xffff << 48) != 0 {
                        new_ahead |= 0b1000;
                    };
                    if new_ahead2 & (0xffff << 32) != 0 {
                        new_ahead |= 0b0100;
                    };
                    if new_ahead2 & (0xffff << 16) != 0 {
                        new_ahead |= 0b0010;
                    };
                    if new_ahead2 & (0xffff << 0) != 0 {
                        new_ahead |= 0b0001;
                    };

                    new_node.ahead = new_ahead;
                    new_node.ahead2 = new_ahead2;

                    // Maintain the invariant that distances don't decrease as
                    // we move away from the root.
                    // We only save the distance for look_ahead rows, so the
                    // distance can decrease again.

                    // The reason for this invariant is that we want to
                    // guarantee that if we fall under max_distance at this
                    // level our parents will fall under max_distance in
                    // future passes even though we propagate distance up to
                    // the parents (in calc_cells).

                    // Midline and arena edge slices always have a distance of 0
                    // and do not go through the logic below.
                    if update_dist {
                        if sp + ahead_rows <= tree_rows {
                            new_node.dist = 0;
                        } else {
                            new_node.dist = max(new_dist, lvl[sp - 1].node.dist);
                        };
                    };

                    node[tp] = new_node;
                    lvl[sp].matches = matches;

                    if sp < tree_rows && new_ahead & 0x0f != 0 {
                        let newtp = node[tp].children();
                        lvl[sp].node = node[tp];
                        lvl[sp].tp = tp;
                        lvl[sp].child = child;
                        tp = newtp;
                        child = lvl[sp].node.child.trailing_zeros() as u8;
                        lvl[sp].new_ahead_mask = 0xf0;
                        lvl[sp].new_ahead_mask2 = 0xf0;
                        sp += 1;
                        if child < 4 {
                            continue;
                        };
                    };
                };
            } else {
                // save_tree_level will ignore this node, but setting
                // ahead to 0 here may allow 0 to bubble up the tree.
                node[tp].ahead &= 0xf0;
            };

            while sp > 0 {
                if child < 4 {
                    if lookbacks[sp].middlerow_future <= tree_rows {
                        node[tp].ahead &= lvl[sp].new_ahead_mask2;
                    };
                    if sp + ahead_rows <= tree_rows {
                        node[tp].ahead &= lvl[sp].new_ahead_mask;
                    };
                    if sp >= ahead_rows && node[tp].ahead & 0x0f != 0 {
                        lvl[sp - ahead_rows].new_ahead_mask |= 1 << child;
                    };

                    // set ahead cells in the middlerow node that have
                    // corresponding cells in the ahead2 lookahead
                    let new_ahead2 = if node[tp].ahead & 0b0001 != 0 {
                        node[tp].ahead2
                    } else {
                        0
                    } | if node[tp].ahead & 0b0010 != 0 {
                        node[tp].ahead2 >> 16
                    } else {
                        0
                    } | if node[tp].ahead & 0b0100 != 0 {
                        node[tp].ahead2 >> 32
                    } else {
                        0
                    } | if node[tp].ahead & 0b1000 != 0 {
                        node[tp].ahead2 >> 48
                    } else {
                        0
                    };
                    let new_ahead_mask2 = if new_ahead2 & 0x000f != 0 { 0b0001 } else { 0 }
                        | if new_ahead2 & 0x00f0 != 0 { 0b0010 } else { 0 }
                        | if new_ahead2 & 0x0f00 != 0 { 0b0100 } else { 0 }
                        | if new_ahead2 & 0xf000 != 0 { 0b1000 } else { 0 };
                    lvl[lookbacks[sp].middlerow].new_ahead_mask2 |= new_ahead_mask2;
                    tp += 1;
                    child += 1;
                    child += (lvl[sp - 1].node.child >> child).trailing_zeros() as u8;
                    if child < 4 {
                        break;
                    };
                };

                // if all of the children have empty ahead masks
                // bubble the empty mask up to the parent.
                let mut nonzero_ahead = false;
                tp = node[lvl[sp - 1].tp].children();
                for _ in 0..(lvl[sp - 1].node.child & 0x0f).count_ones() {
                    if node[tp].ahead & 0x0f != 0 {
                        nonzero_ahead = true;
                    };
                    tp += 1;
                }

                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
                if !nonzero_ahead {
                    node[tp].ahead &= 0xf0;
                };
            }
        }
    }

    // Find the slice that that is the closest to the far edge.
    // The idea is to improve the odds of returning
    // the slice that forms part of a spaceship if there is one.
    pub fn best_partial(&mut self, root: usize, tree_rows: usize, ahead_rows: usize) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let mut tp = node[root].children();
        let mut child = 0u8;
        let mut sp = 1usize;

        lvl[0].tp = root;
        lvl[0].child = 0;
        lvl[0].node = node[root];

        while sp > 0 {
            if lvl[sp - 1].node.child & (1 << child) != 0 && node[tp].ahead & 0x0f != 0 {
                if sp >= ahead_rows && lvl[sp - ahead_rows].node.ahead & (1 << child) == 0 {
                    // disfavor nodes that are not allowed by the ahead mask.
                    node[tp].dist = 255;
                };
                if sp < tree_rows {
                    let newtp = node[tp].children();
                    lvl[sp].node = node[tp];
                    lvl[sp].tp = tp;
                    lvl[sp].child = child;
                    tp = newtp;
                    child = 0;
                    sp += 1;
                    continue;
                };
            };

            while sp > 0 {
                if lvl[sp - 1].node.child & (1 << child) != 0 {
                    tp += 1;
                };
                if child < 3 {
                    child += 1;
                    break;
                };

                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;

                // find best
                let mut best_dist = 255;
                let mut childp = node[tp].children();
                for c in 0..4 {
                    if node[tp].child & (1 << c) != 0 {
                        if node[childp].ahead & 0x0f != 0 {
                            best_dist = min(best_dist, node[childp].dist);
                        };
                        childp += 1;
                    };
                }

                let mut best_found = false;
                let mut childp = node[tp].children();
                for c in 0..4 {
                    if node[tp].child & (1 << c) != 0 {
                        if !best_found
                            && node[childp].ahead & 0x0f != 0
                            && node[childp].dist == best_dist
                        {
                            best_found = true;
                            // Ahead processing is counterproductive for
                            // partials.
                            // It can (correctly) reject all joins to
                            // continue a partial, but it is better
                            // to find a full-width partial even if
                            // there are no possible next rows than to
                            // abandon the partial part-way.
                            node[childp].ahead |= 0x0f;
                            node[childp].ahead2 = u64::max_value();
                        } else {
                            node[childp].ahead &= 0xf0;
                        };
                        node[childp].dist = 0;
                        childp += 1;
                    };
                }
                node[tp].dist = max(node[tp].dist, best_dist);
            }
        }
    }

    pub fn print_partial(
        &mut self,
        root: usize,
        tree_rows: usize,
        ahead_rows: usize,
        lookbacks: &[LvlLookBack],
    ) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let mut tp = node[root].children();
        let mut child = 1u8;
        let mut sp = 1usize;

        lvl[0].tp = root;
        lvl[0].child = 1;
        lvl[0].node = node[root];

        while sp > 0 {
            if lvl[sp - 1].node.child & child != 0
                && node[tp].ahead & 0x0f != 0
                && (sp < ahead_rows || lvl[sp - ahead_rows].node.ahead & child != 0)
            {
                if lookbacks[sp].gen == 0 {
                    if child & 0b1100 != 0 {
                        print!("*");
                    } else {
                        print!(" ");
                    };
                };
                if sp < tree_rows {
                    let newtp = node[tp].children();
                    lvl[sp].node = node[tp];
                    lvl[sp].tp = tp;
                    lvl[sp].child = child;
                    tp = newtp;
                    child = 1;
                    sp += 1;
                    continue;
                };
            };

            while sp > 0 {
                if lvl[sp - 1].node.child & child != 0 {
                    tp += 1;
                };
                if child < 8 {
                    child <<= 1;
                    break;
                };

                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
            }
        }

        println!();
    }

    pub fn exclude(
        &mut self,
        root: usize,
        tree_rows: usize,
        ahead_rows: usize,
        lookbacks: &[LvlLookBack],
        left_pat: &[char],
        right_pat: &[char],
    ) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let mut tp = node[root].children();
        let mut child = 1u8;
        let mut sp = 1usize;
        let mut pat_row = 0usize;

        lvl[0].tp = root;
        lvl[0].child = 1;
        lvl[0].node = node[root];

        while sp > 0 {
            if lvl[sp - 1].node.child & child != 0
                && node[tp].ahead & 0x0f != 0
                && (sp < ahead_rows || lvl[sp - ahead_rows].node.ahead & child != 0)
            {
                let mut pat_matches = true;
                if lookbacks[sp].gen == 0 {
                    if pat_row < left_pat.len() {
                        if left_pat[pat_row] == '*' {
                            if child & 0b1100 == 0 {
                                pat_matches = false;
                            };
                        } else if child & 0b1100 != 0 {
                            pat_matches = false;
                        };
                    };
                    if pat_row < right_pat.len() {
                        if right_pat[pat_row] == '*' {
                            if child & 0b1010 == 0 {
                                pat_matches = false;
                            };
                        } else if child & 0b1010 != 0 {
                            pat_matches = false;
                        };
                    };
                };
                if sp < tree_rows && pat_matches {
                    if lookbacks[sp].gen == 0 {
                        pat_row += 1;
                        if pat_row >= left_pat.len() && pat_row >= right_pat.len() {
                            //exclude this branch.
                            node[tp].ahead &= 0xf0;
                        };
                    };
                    let newtp = node[tp].children();
                    lvl[sp].node = node[tp];
                    lvl[sp].tp = tp;
                    lvl[sp].child = child;
                    tp = newtp;
                    child = 1;
                    sp += 1;
                    continue;
                };
            };

            while sp > 0 {
                if lvl[sp - 1].node.child & child != 0 {
                    tp += 1;
                };
                if child < 8 {
                    child <<= 1;
                    break;
                };

                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
                if lookbacks[sp].gen == 0 {
                    pat_row -= 1;
                };
            }
        }
    }

    fn load_tree_level<D: Decompress>(
        &mut self,
        decompress: &mut D,
        root_row: usize,
        row: usize,
        ahead_rows: usize,
        middle_row: usize,
        head_row: usize,
        middle_row_prev: usize,
        load_ahead2: bool,
    ) {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let mut tp = lvl[root_row].tp;

        if row == root_row {
            node[tp].ahead = decompress.decode(
                lvl[root_row].child + (lvl[middle_row].child << 2) + (lvl[head_row].child << 4),
            );
            if load_ahead2 {
                node[tp].dist = decompress.decode(66) << 4;
                node[tp].dist += decompress.decode(64 + ((node[tp].dist >> 4) & 1));
                // We could support the full u8 distances allowed by the format
                // by using more care to avoid overflows, but there is no
                // point in doing so; saturating at 127 could potentially
                // lead to different partials being chosen, but is plenty for
                // the max distance in the join.
                node[tp].dist = min(node[tp].dist, 127);
                node[tp].ahead2 = 0;

                let mut b45 = node[tp].ahead.trailing_zeros();
                while b45 < 4 {
                    for b23 in 0..4 {
                        node[tp].ahead2 |= (decompress
                            .decode(b45 as u8 + (b23 << 2) as u8 + lvl[middle_row_prev].child)
                            as u64)
                            << ((b45 << 4) + (b23 << 2))
                    }
                    b45 += 1;
                    b45 += (node[tp].ahead >> b45).trailing_zeros();
                }
            };
            if ahead_rows == 1 {
                node[tp].child = node[tp].ahead;
            } else {
                node[tp].child = 0x0f;
            };
            return;
        };

        lvl[root_row].node = node[tp];

        if root_row + 1 == row {
            let children = node.len();
            node[tp].set_children(children);
        };
        tp = node[tp].children();
        let mut child = lvl[root_row].node.child.trailing_zeros() as u8;
        let mut sp = root_row + 1;

        if child >= 4 {
            return;
        };

        while sp > root_row {
            if sp < row {
                if sp + 1 == row {
                    let newtp = node.len();
                    node[tp].set_children(newtp);
                };
                let newtp = node[tp].children();
                lvl[sp].node = node[tp];
                lvl[sp].tp = tp;
                lvl[sp].child = child;
                tp = newtp;
                child = lvl[sp].node.child.trailing_zeros() as u8;
                sp += 1;
                if child < 4 {
                    continue;
                } else {
                    child = 4;
                };
            } else {
                let ahead_mask = decompress
                    .decode(child + (lvl[middle_row].child << 2) + (lvl[head_row].child << 4));

                if ahead_mask != 0 {
                    let child_mask = if ahead_rows == 1 {
                        ahead_mask
                    } else if sp + 1 >= ahead_rows {
                        lvl[sp + 1 - ahead_rows].node.ahead & 0x0f
                    } else {
                        0x0fu8
                    };
                    let mut new_node =
                        TreeNode::new(child_mask, ahead_mask, u64::max_value(), false);
                    if load_ahead2 {
                        let prev_mask = if middle_row >= root_row {
                            node[lvl[middle_row].tp].ahead
                        } else {
                            0x0f
                        };
                        new_node.dist = decompress.decode(66) << 4;
                        new_node.dist += decompress.decode(64 + ((new_node.dist >> 4) & 1));
                        new_node.dist = min(new_node.dist, 127);
                        new_node.ahead2 = 0;

                        let mut b45 = ahead_mask.trailing_zeros();
                        while b45 < 4 {
                            let mut b23 = prev_mask.trailing_zeros();
                            while b23 < 4 {
                                new_node.ahead2 |= (decompress.decode(
                                    b45 as u8 + (b23 << 2) as u8 + lvl[middle_row_prev].child,
                                ) as u64)
                                    << ((b45 << 4) + (b23 << 2));
                                b23 += 1;
                                b23 += (prev_mask >> b23).trailing_zeros();
                            }
                            b45 += 1;
                            b45 += (ahead_mask >> b45).trailing_zeros();
                        }
                    };
                    node.push(new_node);
                } else {
                    let parent_tp = lvl[sp - 1].tp;
                    node[parent_tp].child &= !(1 << child);
                };
            };

            while sp > 0 {
                tp += 1;
                child += 1;
                child += (lvl[sp - 1].node.child >> child).trailing_zeros() as u8;
                if child < 4 {
                    break;
                };
                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
            }
        }
    }

    pub fn chunks_to_tree(
        &mut self,
        chunks: &[usize],
        raw: &[u8],
        desc: &SliceDesc,
        ahead_rows: usize,
        lookbacks: &[LvlLookBack],
    ) -> usize {
        let root: usize = self.node.len();
        self.lvl[0].node = TreeNode::new(0, 0, u64::max_value(), true);
        self.node.push(self.lvl[0].node);

        self.lvl[0].tp = root;
        self.lvl[0].child = 0;

        // key_cache corresponds to the part of the tree that is currently
        // loaded into lvl[i].node/tp/child.
        // We can use this information to avoid walking from the root for
        // every chunk.
        self.key_cache[0].clear();
        self.key_cache[1].clear();

        for &chunk in chunks {
            let header = &raw[chunk..chunk + desc.chunk_head_size];
            let key_rows = desc.slice_rows - (header[4] & 127) as usize;

            let mut diff_start = 0;
            let diff_limit = min(key_rows >> 3, self.key_cache[0].len());
            loop {
                if diff_start >= diff_limit
                    || self.key_cache[0][diff_start] != header[5 + diff_start]
                    || self.key_cache[1][diff_start] != header[5 + desc.key_bytes + diff_start]
                {
                    break;
                };
                diff_start += 1;
            }

            for i in 8 * diff_start..key_rows {
                if self.lvl[i].node.child == 0 {
                    // We don't have a way of adding children later, so add
                    // them all now. Children we don't use will themselves
                    // have child_mask = 0 and including them in the tree
                    // is low cost.
                    self.lvl[i].node = TreeNode::new(0x0f, 0x0f, u64::max_value(), true);
                    let children = self.node.len();
                    self.lvl[i].node.set_children(children);
                    let tp = self.lvl[i].tp;
                    self.node[tp] = self.lvl[i].node;
                    for _ in 0..4 {
                        self.node.push(TreeNode::new(0, 0, u64::max_value(), false));
                    }
                };
                self.lvl[i + 1].tp = self.node[self.lvl[i].tp].children();
                self.lvl[i + 1].child = 0;

                if header[5 + (i >> 3)] & (0x80u8 >> (i & 7)) != 0 {
                    self.lvl[i + 1].child += 2;
                    self.lvl[i + 1].tp += 2;
                };
                if header[5 + desc.key_bytes + (i >> 3)] & (0x80u8 >> (i & 7)) != 0 {
                    self.lvl[i + 1].child += 1;
                    self.lvl[i + 1].tp += 1;
                };
                self.lvl[i + 1].node = self.node[self.lvl[i + 1].tp];
            }
            if header[4] & 128 != 0 {
                let tp = self.lvl[key_rows].tp;
                self.node[tp].setchunkhead();
            };

            let slice_data = &raw[chunk + desc.chunk_head_size
                ..chunk
                    + desc.chunk_head_size
                    + u32::from_be_bytes(header[..4].try_into().unwrap()) as usize];

            match desc.compression {
                Compression::Packed => {
                    let mut decompress = PackedDecompressor::new(slice_data);

                    for lvls in key_rows..=desc.slice_rows {
                        self.load_tree_level(
                            &mut decompress,
                            key_rows,
                            lvls,
                            ahead_rows,
                            lookbacks[lvls].middlerow,
                            lookbacks[lvls].headrow,
                            lookbacks[lookbacks[lvls].middlerow].middlerow,
                            desc.slice_rows - lvls < ahead_rows,
                        );
                    }

                    decompress.finish();
                }
                Compression::Arithmetic => {
                    let mut decompress = ArithmeticDecompressor::new(slice_data);

                    for lvls in key_rows..=desc.slice_rows {
                        self.load_tree_level(
                            &mut decompress,
                            key_rows,
                            lvls,
                            ahead_rows,
                            lookbacks[lvls].middlerow,
                            lookbacks[lvls].headrow,
                            lookbacks[lookbacks[lvls].middlerow].middlerow,
                            desc.slice_rows - lvls < ahead_rows,
                        );
                    }

                    decompress.finish();
                }
                Compression::Rle => {
                    let mut decompress = RleDecompressor::new(slice_data);

                    for lvls in key_rows..=desc.slice_rows {
                        self.load_tree_level(
                            &mut decompress,
                            key_rows,
                            lvls,
                            ahead_rows,
                            lookbacks[lvls].middlerow,
                            lookbacks[lvls].headrow,
                            lookbacks[lookbacks[lvls].middlerow].middlerow,
                            desc.slice_rows - lvls < ahead_rows,
                        );
                    }

                    decompress.finish();
                }
            };

            self.key_cache[0].clear();
            self.key_cache[0].extend_from_slice(&header[5..5 + (key_rows >> 3)]);
            self.key_cache[1].clear();
            self.key_cache[1].extend_from_slice(
                &header[5 + desc.key_bytes..5 + desc.key_bytes + (key_rows >> 3)],
            );

            for i in 0..4 {
                if self.size() as isize > self.large_threshold[i] {
                    if self.large_guard[i].is_none() {
                        self.large_guard[i] = Some(self.large_sem[i].access());
                    };
                } else {
                    break;
                };
            }
        }
        root
    }

    fn new_ahead(node: &TreeNode, middlenode: &TreeNode) -> u8 {
        let mut rv = node.ahead;
        let mut ahead2 = node.ahead2;
        if middlenode.ahead & 0b0001 == 0 {
            ahead2 &= 0xfff0_fff0_fff0_fff0;
        };
        if middlenode.ahead & 0b0010 == 0 {
            ahead2 &= 0xff0f_ff0f_ff0f_ff0f;
        };
        if middlenode.ahead & 0b0100 == 0 {
            ahead2 &= 0xf0ff_f0ff_f0ff_f0ff;
        };
        if middlenode.ahead & 0b1000 == 0 {
            ahead2 &= 0x0fff_0fff_0fff_0fff;
        };
        if ahead2 & 0x0000_0000_0000_ffff == 0 {
            rv &= 0b1111_1110;
        };
        if ahead2 & 0x0000_0000_ffff_0000 == 0 {
            rv &= 0b1111_1101;
        };
        if ahead2 & 0x0000_ffff_0000_0000 == 0 {
            rv &= 0b1111_1011;
        };
        if ahead2 & 0xffff_0000_0000_0000 == 0 {
            rv &= 0b1111_0111;
        };
        rv
    }

    // Writes a chunk level
    // returning the number of nonempty leaf nodes written.
    //
    // This function also propagates changes to lookahead information
    // to future nodes. Doing it here means we can pick up changes
    // that join() propagated from the tail without having to do a separate
    // walk for the propagation.
    //
    pub fn save_tree_level<C: Compress>(
        &mut self,
        compress: &mut C,
        root_row: usize,
        row: usize,
        ahead_rows: usize,
        middle_row: usize,
        head_row: usize,
        middle_row_prev: usize,
        save_ahead2: bool,
    ) -> u64 {
        let lvl = &mut self.lvl;
        let node = &mut self.node;
        let mut tp = lvl[root_row].tp;
        let mut slice_count = 0;

        if row == root_row {
            node[tp].ahead = Workspace::new_ahead(&node[tp], &lvl[middle_row].node);
            compress.encode(
                lvl[root_row].child + (lvl[middle_row].child << 2) + (lvl[head_row].child << 4),
                node[tp].ahead & 0x0f,
            );
            if save_ahead2 {
                compress.encode(66, node[tp].dist >> 4);
                compress.encode(64 + ((node[tp].dist >> 4) & 1), node[tp].dist & 0x0f);
                let mut b45 = node[tp].ahead.trailing_zeros();
                while b45 < 4 {
                    for b23 in 0..4 {
                        compress.encode(
                            b45 as u8 + (b23 << 2) as u8 + lvl[middle_row_prev].child,
                            (node[tp].ahead2 >> ((b45 << 4) + (b23 << 2))) as u8 & 0x0f,
                        );
                    }
                    b45 += 1;
                    b45 += (node[tp].ahead >> b45).trailing_zeros();
                }
            };
            lvl[root_row].node = node[tp];
            return 1;
        };

        if node[tp].ahead & 0x0f == 0 {
            return 0;
        };

        tp = node[tp].children();
        let mut child = 0u8;
        let mut sp = root_row + 1;

        while sp > root_row {
            if sp < row {
                if lvl[sp - 1].node.child & (1 << child) != 0 &&
                    (sp < ahead_rows ||
                    lvl[sp - ahead_rows].node.ahead & (1 << child) != 0
                    ) &&
                    // load_tree_level doesn't add nodes with
                    // ahead & 0x0f == 0 to the tree
                    node[tp].ahead & 0x0f != 0
                {
                    let newtp = node[tp].children();
                    lvl[sp].node = node[tp];
                    lvl[sp].tp = tp;
                    lvl[sp].child = child;
                    tp = newtp;
                    child = 0;
                    sp += 1;
                    continue;
                };
            } else if sp < ahead_rows || lvl[sp - ahead_rows].node.ahead & (1 << child) != 0 {
                if lvl[sp - 1].node.child & (1 << child) != 0 {
                    node[tp].ahead = Workspace::new_ahead(&node[tp], &lvl[middle_row].node);
                    compress.encode(
                        child + (lvl[middle_row].child << 2) + (lvl[head_row].child << 4),
                        node[tp].ahead & 0x0f,
                    );
                    if save_ahead2 && node[tp].ahead & 0x0f != 0 {
                        let prev_mask = if middle_row >= root_row {
                            node[lvl[middle_row].tp].ahead
                        } else {
                            0x0f
                        };
                        compress.encode(66, node[tp].dist >> 4);
                        compress.encode(64 + ((node[tp].dist >> 4) & 1), node[tp].dist & 0x0f);

                        let mut b45 = node[tp].ahead.trailing_zeros();
                        while b45 < 4 {
                            let mut b23 = prev_mask.trailing_zeros();
                            while b23 < 4 {
                                compress.encode(
                                    b45 as u8 + (b23 << 2) as u8 + lvl[middle_row_prev].child,
                                    (node[tp].ahead2 >> ((b45 << 4) + (b23 << 2))) as u8 & 0x0f,
                                );
                                b23 += 1;
                                b23 += (prev_mask >> b23).trailing_zeros();
                            }
                            b45 += 1;
                            b45 += (node[tp].ahead >> b45).trailing_zeros();
                        }
                    };
                    slice_count += 1;
                } else {
                    // we need to save an empty ahead_mask
                    // to keep load tree state and save tree state
                    // in sync.
                    compress.encode(
                        child + (lvl[middle_row].child << 2) + (lvl[head_row].child << 4),
                        0,
                    );
                };
            } else if sp < root_row + ahead_rows {
                // we need to save an empty ahead_mask
                // to keep load tree state and save tree state
                // in sync.
                compress.encode(
                    child + (lvl[middle_row].child << 2) + (lvl[head_row].child << 4),
                    0,
                );
            };

            while sp > 0 {
                if lvl[sp - 1].node.child & (1 << child) != 0 {
                    tp += 1;
                };
                if child < 3 {
                    child += 1;
                    break;
                };
                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
            }
        }
        slice_count
    }

    // Serializes tree into chunks
    // returning the number of slices (nonempty leaf nodes) written.
    pub fn tree_to_chunks(
        &mut self,
        root: usize,
        desc: &SliceDesc,
        ahead_rows: usize,
        chunks: &mut Vec<usize>,
        raw: &mut Vec<u8>,
        lookbacks: &[LvlLookBack],
        min_key_rows: usize,
    ) -> u64 {
        let mut tp = self.node[root].children();
        let mut child = 0u8;
        let mut sp = 1usize;
        let mut slice_count = 0;

        self.lvl[0].tp = root;
        self.lvl[0].child = 0;
        self.lvl[0].node = self.node[root];
        self.lvl[1].tp = self.node[root].children();
        self.lvl[1].child = 0;

        if self.lvl[0].node.ahead & 0x0f == 0 {
            return 0;
        }

        let mut header: usize;

        header = raw.len();
        // push chunk header
        raw.resize(header + desc.chunk_head_size, 0);

        while sp > 0 {
            if self.lvl[sp - 1].node.child & (1 << child) != 0 && self.node[tp].ahead & 0x0f != 0 {
                let key_bit = 0x80u8 >> ((sp - 1) & 7);
                //mask out bits for previous level
                raw[header + 5 + ((sp - 1) >> 3)] &= !key_bit;
                raw[header + 5 + desc.key_bytes + ((sp - 1) >> 3)] &= !key_bit;

                if child > 1 {
                    raw[header + 5 + ((sp - 1) >> 3)] |= key_bit;
                };
                if (child & 1) != 0 {
                    raw[header + 5 + desc.key_bytes + ((sp - 1) >> 3)] |= key_bit;
                };

                if sp < min_key_rows
                    || (self.node[tp].chunkhead() && sp + ahead_rows <= desc.slice_rows)
                {
                    let newtp = self.node[tp].children();
                    self.lvl[sp].node = self.node[tp];
                    self.lvl[sp].tp = tp;
                    self.lvl[sp].child = child;
                    tp = newtp;
                    child = 0;
                    sp += 1;
                    continue;
                } else {
                    let mut last_row_slice_count = 0;
                    self.lvl[sp].tp = tp;
                    self.lvl[sp].child = child;
                    match desc.compression {
                        Compression::Packed => {
                            let mut compress = PackedCompressor::new(raw);
                            for row in sp..=desc.slice_rows {
                                let chunk_slice_count = self.save_tree_level(
                                    &mut compress,
                                    sp,
                                    row,
                                    ahead_rows,
                                    lookbacks[row].middlerow,
                                    lookbacks[row].headrow,
                                    lookbacks[lookbacks[row].middlerow].middlerow,
                                    desc.slice_rows - row < ahead_rows,
                                );
                                if row == desc.slice_rows {
                                    last_row_slice_count = chunk_slice_count;
                                };
                            }
                            compress.finish();
                        }
                        Compression::Arithmetic => {
                            let mut compress = ArithmeticCompressor::new(raw);
                            for row in sp..=desc.slice_rows {
                                let chunk_slice_count = self.save_tree_level(
                                    &mut compress,
                                    sp,
                                    row,
                                    ahead_rows,
                                    lookbacks[row].middlerow,
                                    lookbacks[row].headrow,
                                    lookbacks[lookbacks[row].middlerow].middlerow,
                                    desc.slice_rows - row < ahead_rows,
                                );
                                if row == desc.slice_rows {
                                    last_row_slice_count = chunk_slice_count;
                                };
                            }
                            compress.finish();
                        }
                        Compression::Rle => {
                            let mut compress = RleCompressor::new(raw);
                            for row in sp..=desc.slice_rows {
                                let chunk_slice_count = self.save_tree_level(
                                    &mut compress,
                                    sp,
                                    row,
                                    ahead_rows,
                                    lookbacks[row].middlerow,
                                    lookbacks[row].headrow,
                                    lookbacks[lookbacks[row].middlerow].middlerow,
                                    desc.slice_rows - row < ahead_rows,
                                );
                                if row == desc.slice_rows {
                                    last_row_slice_count = chunk_slice_count;
                                };
                            }
                            compress.finish();
                        }
                    };

                    if last_row_slice_count > 0 {
                        slice_count += last_row_slice_count;
                        let datasize = raw.len() - header - desc.chunk_head_size;
                        chunks.push(header);
                        raw[header..header + 4]
                            .copy_from_slice(&u32::try_from(datasize).unwrap().to_be_bytes());
                        raw[header + 4] = (desc.slice_rows - sp) as u8;
                        if sp == min_key_rows {
                            raw[header + 4] |= 128;
                        };
                        let nextheader = raw.len();
                        raw.resize(nextheader + desc.chunk_head_size, 0);
                        let (raw2, newhead) = raw.split_at_mut(nextheader);
                        newhead.copy_from_slice(&raw2[header..header + desc.chunk_head_size]);
                        header = nextheader;
                    } else {
                        raw.truncate(header + desc.chunk_head_size);
                    };
                };
            };

            while sp > 0 {
                if self.lvl[sp - 1].node.child & (1 << child) != 0 {
                    tp += 1;
                };
                if child < 3 {
                    child += 1;
                    break;
                };

                let key_bit = 0x80u8 >> ((sp - 1) & 7);
                // key bits will be overwritten except when the
                // next chunk has fewer key bits. This is benign,
                // but masking them here means that we won't sort
                // on junk bits, and will make tail compression
                // slightly more effective.
                raw[header + 5 + ((sp - 1) >> 3)] &= !key_bit;
                raw[header + 5 + desc.key_bytes + ((sp - 1) >> 3)] &= !key_bit;

                sp -= 1;
                tp = self.lvl[sp].tp;
                child = self.lvl[sp].child;
            }
        }
        raw.resize(header, 0);
        slice_count
    }

    pub fn visit_tree(
        &mut self,
        root: usize,
        tree_rows: usize,
        mut visitor: impl FnMut(usize, u8, u8, u64, u8),
    ) {
        let lvl = &mut self.lvl;
        let node = &self.node;

        lvl[0].node = node[root];

        let mut tp = self.node[root].children();
        let mut child = lvl[0].node.child.trailing_zeros() as u8;
        let mut sp = 1usize;

        if node[root].ahead & 0x0f != 0 {
            visitor(
                0,
                0,
                node[root].ahead & 0x0f,
                node[root].ahead2,
                node[root].dist,
            );
        };

        while sp > 0 {
            if node[tp].ahead & 0x0f != 0 {
                visitor(
                    sp,
                    child,
                    node[tp].ahead & 0x0f,
                    node[tp].ahead2,
                    node[tp].dist,
                );
                if sp < tree_rows {
                    let newtp = node[tp].children();
                    lvl[sp].node = node[tp];
                    lvl[sp].tp = tp;
                    lvl[sp].child = child;
                    child = node[tp].child.trailing_zeros() as u8;
                    tp = newtp;
                    sp += 1;
                    if child < 4 {
                        continue;
                    };
                };
            };

            while sp > 0 {
                if child < 4 {
                    tp += 1;
                    child += 1;
                    child += (lvl[sp - 1].node.child >> child).trailing_zeros() as u8;
                    if child < 4 {
                        break;
                    };
                };
                sp -= 1;
                tp = lvl[sp].tp;
                child = lvl[sp].child;
            }
        }
    }
}
