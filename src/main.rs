mod chunkio;
mod compress;
mod workspace;

use std::convert::TryInto;

use chunkio::Compression;
use chunkio::InMut;
use chunkio::OutMut;
use chunkio::SliceDesc;
use chunkio::SortSide;

use workspace::Lookups;
use workspace::LvlLookBack;
use workspace::Workspace;

use getopts::Matches;
use getopts::Options;
use std::env;
use std::error;
use std::io;

use std::cmp::{max, min};

use std::str;
use std::sync::{Arc, Mutex};
use std::thread;

use std_semaphore::Semaphore;

fn parse_opt<F: str::FromStr>(opt: &Matches, opt_name: &str) -> Result<F, String>
where
    <F as str::FromStr>::Err: std::string::ToString,
{
    opt.opt_str(opt_name)
        .ok_or_else(|| format!("Expected parameter --{} not found", opt_name))?
        .parse::<F>()
        .map_err(|err| format!("Invalid Value for --{}: '{}'", opt_name, err.to_string()))
}

fn parse_opt_or<F: str::FromStr>(opt: &Matches, opt_name: &str, default: F) -> Result<F, String>
where
    <F as str::FromStr>::Err: std::string::ToString,
{
    if let Some(s) = opt.opt_str(opt_name) {
        s.parse::<F>()
            .map_err(|err| format!("Invalid value for --{}: '{}'", opt_name, err.to_string()))
    } else {
        Ok(default)
    }
}

struct Init {
    desc: SliceDesc,
    seed: String,
    ahead_rows: usize,
    dir: String,
    lookbacks: Box<[LvlLookBack]>,
}

impl Init {
    fn new(opt: &Matches) -> Result<Init, Box<dyn error::Error + Sync + Send>> {
        let mut period = None;
        let mut starts = None;
        let mut shifts = Some(workspace::parse_shifts(
            &opt.opt_str("shifts").unwrap_or_else(|| "".to_string()),
        )?);

        if opt.opt_present("period") {
            period = Some(parse_opt(opt, "period")?);
        };

        if opt.opt_present("starts") {
            starts = Some(workspace::parse_starts(&opt.opt_str("starts").unwrap())?);
        };

        if opt.opt_present("velocity") {
            let (period_, starts_, shifts_) =
                workspace::parse_velocity(&opt.opt_str("velocity").unwrap())?;
            period = Some(period_);
            starts = Some(starts_);
            shifts = Some(shifts_);
        };

        let slice_rows = parse_opt::<usize>(opt, "slice_rows")?;

        let desc = SliceDesc::new(
            slice_rows,
            SortSide::opt_parse(opt, "sort")?,
            if opt.opt_present("arithmetic") {
                Compression::Arithmetic
            } else if opt.opt_present("compress") {
                Compression::Rle
            } else {
                Compression::Packed
            },
        );

        Ok(Init {
            desc,
            seed: opt.opt_str("seed").unwrap(),
            ahead_rows: starts.as_ref().unwrap().len(),
            dir: opt.opt_str("out_dir").unwrap(),
            lookbacks: LvlLookBack::prepare(
                starts.as_ref().unwrap(),
                shifts.as_ref().unwrap(),
                slice_rows,
                period.unwrap(),
            ),
        })
    }

    fn run(&self) -> io::Result<()> {
        let mut out = OutMut::new(self.dir.clone(), &self.desc, 1 << 30);
        let mut raw = Vec::<u8>::new();
        let mut chunks = Vec::<usize>::new();
        let large_sem = [
            Semaphore::new(1),
            Semaphore::new(1),
            Semaphore::new(1),
            Semaphore::new(1),
        ];
        let mut workspace = Workspace::new(self.desc.slice_rows, &large_sem);

        let seed: Vec<char> = self.seed.chars().collect();

        let root = workspace.seed_tree(&seed[..], self.desc.slice_rows, self.ahead_rows);
        let slice_count = workspace.tree_to_chunks(
            root,
            &self.desc,
            self.ahead_rows,
            &mut chunks,
            &mut raw,
            &*self.lookbacks,
            self.desc.slice_rows - 8,
        );
        out.extend(&chunks, &raw, &self.desc, slice_count)?;
        out.flush_out(&self.desc)?;
        Ok(())
    }
}

struct InIO {
    left: InMut,
    right: InMut,
    key_upper: Box<[u8]>,
}

struct OutIO {
    left: Option<OutMut>,
    right: Option<OutMut>,
}

struct Join {
    in_io: Mutex<InIO>,
    out_io: Mutex<OutIO>,
    in_left: SliceDesc,
    in_right: SliceDesc,
    out_left: Option<SliceDesc>,
    out_right: Option<SliceDesc>,
    extend_mask2_left: u64,
    extend_mask2_right: u64,
    exclude: Box<[Box<[char]>]>,
    key_bytes: usize,
    tree_rows: usize,
    join_rows: usize,
    ahead_rows: usize,
    max_dist: u8,
    max_chunk_rows: usize,
    lookbacks: Box<[LvlLookBack]>,
    lookups_left: Lookups,
    lookups_right: Lookups,
    partial: bool,
    odd_midline: bool,
    print_stats: bool,
    threads: i32,
    workspace_large_threshold: usize,
    split_chunks: bool,
    large_sem: [Semaphore; 4],
}

impl Join {
    fn new(opt: &Matches) -> Result<Join, Box<dyn error::Error + Send + Sync>> {
        let mut period = None;
        let mut starts = None;
        let mut shifts = Some(workspace::parse_shifts(
            &opt.opt_str("shifts").unwrap_or_else(|| "".to_string()),
        )?);

        if opt.opt_present("period") {
            period = Some(parse_opt(opt, "period")?);
        };

        if opt.opt_present("starts") {
            starts = Some(workspace::parse_starts(&opt.opt_str("starts").unwrap())?);
        };

        if opt.opt_present("velocity") {
            let (period_, starts_, shifts_) =
                workspace::parse_velocity(&opt.opt_str("velocity").unwrap())?;
            period = Some(period_);
            starts = Some(starts_);
            shifts = Some(shifts_);
        };

        let (io_in_left, in_left) = InMut::new(&opt.opt_str("in_left").unwrap(), SortSide::Left)?;
        let (io_in_right, in_right) =
            InMut::new(&opt.opt_str("in_right").unwrap(), SortSide::Right)?;

        let out_left = if opt.opt_present("out_left") {
            Some(SliceDesc::new(
                parse_opt_or(opt, "left_rows", in_left.slice_rows)?,
                SortSide::opt_parse(opt, "left_sort")?,
                if opt.opt_present("arithmetic") {
                    Compression::Arithmetic
                } else if opt.opt_present("compress") {
                    Compression::Rle
                } else {
                    Compression::Packed
                },
            ))
        } else {
            None
        };

        let out_right = if opt.opt_present("out_right") {
            Some(SliceDesc::new(
                parse_opt_or(opt, "right_rows", in_right.slice_rows)?,
                SortSide::opt_parse(opt, "right_sort")?,
                if opt.opt_present("arithmetic") {
                    Compression::Arithmetic
                } else if opt.opt_present("compress") {
                    Compression::Rle
                } else {
                    Compression::Packed
                },
            ))
        } else {
            None
        };

        let flush_left_after = parse_opt_or(opt, "flush_left_after", 1 << 30)?;
        let flush_right_after = parse_opt_or(opt, "flush_right_after", 1 << 30)?;
        let key_bytes = *[in_left.key_bytes, in_right.key_bytes]
            .iter()
            .min()
            .unwrap();

        let max_chunk_rows = min(
            parse_opt_or(opt, "max_chunk_rows", 60)?,
            100, // The chunk rows are stored in a u8, with the high bit
                 // indicating that the same chunk rows should be used in the
                 // next pass.
                 // We leave a little margin before reaching 128 since the
                 // calculation can be a bit off when we're outputting both sides.
        );

        let in_io = InIO {
            left: io_in_left,
            right: io_in_right,
            key_upper: vec![0u8; key_bytes].into_boxed_slice(),
        };

        let out_io = OutIO {
            left: out_left
                .as_ref()
                .map(|desc| OutMut::new(opt.opt_str("out_left").unwrap(), desc, flush_left_after)),
            right: out_right.as_ref().map(|desc| {
                OutMut::new(opt.opt_str("out_right").unwrap(), desc, flush_right_after)
            }),
        };

        let threads: i32 = parse_opt_or(opt, "threads", num_cpus::get() as i32)?;
        let mem: usize = parse_opt_or(
            opt,
            "mem",
            out_io.left.as_ref().map_or(0, |d| d.flush_after)
                + out_io.right.as_ref().map_or(0, |d| d.flush_after),
        )?;
        let workspace_large_threshold: usize = min(
            mem / threads as usize,
            // If the workspace gets too large the internal indexes
            // will overflow u32. 0x2_0000_0000 provides a bit of margin
            // as the overflow won't occur until until past
            // 0x10_0000_0000.
            0x2_0000_0000,
        );

        let join_rows = *[
            out_left.as_ref().map_or(0, |d| d.slice_rows),
            out_right.as_ref().map_or(0, |d| d.slice_rows),
        ]
        .iter()
        .max()
        .unwrap();
        let tree_rows = *[join_rows, in_left.slice_rows, in_right.slice_rows]
            .iter()
            .max()
            .unwrap();

        //Finding partial spaceships can fail due to the lookahead logic
        //ruling out all joins. Join fewer rows to avoid this.
        let join_rows = if opt.opt_present("partial") && join_rows > starts.as_ref().unwrap().len()
        {
            join_rows - starts.as_ref().unwrap().len()
        } else {
            join_rows
        };

        let (lookups_left, lookups_right) =
            Lookups::create(&opt.opt_str("rule").unwrap_or_else(|| "B3/S23".to_string()));

        let mut extend_mask2 = opt
            .opt_str("extend_mask")
            .unwrap_or_else(|| "f".to_string())
            .chars()
            .next()
            .unwrap()
            .to_digit(16)
            .unwrap() as u64
            & if opt.opt_present("zero") { 0x1 } else { 0xf }
            & if opt.opt_present("even_midline") {
                0x9
            } else {
                0xf
            }
            & if opt.opt_present("gutter") { 0x3 } else { 0xf }
            & if opt.opt_present("nonzero") { 0xe } else { 0xf };

        extend_mask2 += extend_mask2 << 4;
        extend_mask2 += extend_mask2 << 8;
        extend_mask2 += extend_mask2 << 16;
        extend_mask2 += extend_mask2 << 32;

        // We want enough chunks that all threads are kept busy.
        let split_chunks = (out_left.is_some() && in_io.left.chunks_remaining() < 1 << 16)
            || (out_right.is_some() && in_io.right.chunks_remaining() < 1 << 16);

        Ok(Join {
            in_io: Mutex::new(in_io),
            out_io: Mutex::new(out_io),
            in_left,
            in_right,
            out_left,
            out_right,
            extend_mask2_left: if opt.opt_present("out_left") {
                extend_mask2
            } else {
                u64::max_value()
            },
            extend_mask2_right: if opt.opt_present("out_right") {
                extend_mask2
            } else {
                u64::max_value()
            },
            exclude: if opt.opt_present("exclude") {
                opt.opt_str("exclude")
                    .unwrap()
                    .split('/')
                    .map(|pat| pat.chars().collect::<Box<[char]>>())
                    .collect::<Box<[Box<[char]>]>>()
            } else {
                Box::new([])
            },
            key_bytes,
            tree_rows,
            join_rows,
            ahead_rows: starts.as_ref().unwrap().len(),
            max_dist: min(parse_opt_or(opt, "max_width", 255)?, 100) + 1,
            max_chunk_rows,
            lookbacks: LvlLookBack::prepare(
                starts.as_ref().unwrap(),
                shifts.as_ref().unwrap(),
                tree_rows,
                period.unwrap(),
            ),
            lookups_left,
            lookups_right,
            partial: opt.opt_present("partial"),
            odd_midline: opt.opt_present("odd_midline") || opt.opt_present("gutter"),
            print_stats: opt.opt_present("print_stats"),
            threads,
            workspace_large_threshold,
            split_chunks,
            large_sem: [
                Semaphore::new((threads + 1) as isize / 2),
                Semaphore::new((threads + 3) as isize / 4),
                Semaphore::new((threads + 7) as isize / 8),
                Semaphore::new((threads + 15) as isize / 16),
            ],
        })
    }

    fn do_work(&self) -> io::Result<()> {
        let mut workspace = Workspace::new(self.tree_rows, &self.large_sem);
        let mut left_chunks: Vec<usize> = Vec::new();
        let mut left_raw: Vec<u8> = Vec::new();
        let mut right_chunks: Vec<usize> = Vec::new();
        let mut right_raw: Vec<u8> = Vec::new();

        let mut slice_count_left = 0;
        let mut slice_count_right = 0;

        loop {
            let mut chunk_match_rows = std::usize::MAX;

            // # 1 read in matching chunks
            let mut io = self.in_io.lock().unwrap();

            let mut match_found = false;
            while !match_found {
                match_found = true;
                while !io.left.eof()
                    && io.left.key_upper[0..self.key_bytes] < io.right.key_lower[0..self.key_bytes]
                {
                    match_found = false;
                    io.left.advance(&self.in_left)?;
                }
                while !io.right.eof()
                    && io.right.key_upper[0..self.key_bytes] < io.left.key_lower[0..self.key_bytes]
                {
                    match_found = false;
                    io.right.advance(&self.in_right)?;
                }
            }

            if io.left.eof() || io.right.eof() {
                break;
            };

            left_chunks.clear();
            left_raw.clear();
            right_chunks.clear();
            right_raw.clear();

            {
                let InIO {
                    ref mut key_upper,
                    ref mut left,
                    ..
                } = *io;
                key_upper.copy_from_slice(&left.key_upper[0..self.key_bytes]);
            }

            let mut key_upper_changed = true;
            while key_upper_changed {
                key_upper_changed = false;

                while !io.left.eof() && io.left.key_lower[0..self.key_bytes] <= *io.key_upper {
                    if io.left.key_upper[0..self.key_bytes] > *io.key_upper {
                        let InIO {
                            ref mut key_upper,
                            ref mut left,
                            ..
                        } = *io;
                        key_upper.copy_from_slice(&left.key_upper[0..self.key_bytes]);
                        key_upper_changed = true;
                    };
                    if self.in_left.slice_rows - ((io.left.header()[4] & 127) as usize)
                        < chunk_match_rows
                    {
                        chunk_match_rows =
                            self.in_left.slice_rows - (io.left.header()[4] & 127) as usize;
                    };
                    io.left
                        .read_chunk(&self.in_left, &mut left_chunks, &mut left_raw)?;
                }

                while !io.right.eof() && io.right.key_lower[0..self.key_bytes] <= *io.key_upper {
                    if io.right.key_upper[0..self.key_bytes] > *io.key_upper {
                        let InIO {
                            ref mut key_upper,
                            ref mut right,
                            ..
                        } = *io;
                        key_upper.copy_from_slice(&right.key_upper[0..self.key_bytes]);
                        key_upper_changed = true;
                    };
                    if self.in_right.slice_rows - ((io.right.header()[4] & 127) as usize)
                        < chunk_match_rows
                    {
                        chunk_match_rows =
                            self.in_right.slice_rows - (io.right.header()[4] & 127) as usize;
                    };
                    io.right
                        .read_chunk(&self.in_right, &mut right_chunks, &mut right_raw)?;
                }
            }

            // #2 Drop mutex. After this point we run parallel.
            drop(io);

            // #3 Prepare trees.
            workspace.clear();

            let input_buf_size = left_raw.len() + right_raw.len();
            workspace.large_threshold[0] =
                self.workspace_large_threshold as isize - input_buf_size as isize;
            workspace.large_threshold[1] =
                self.workspace_large_threshold as isize * 5 / 4 - input_buf_size as isize;
            workspace.large_threshold[2] =
                self.workspace_large_threshold as isize * 7 / 4 - input_buf_size as isize;
            workspace.large_threshold[3] =
                self.workspace_large_threshold as isize * 4 - input_buf_size as isize;

            let left_tree = workspace.chunks_to_tree(
                &left_chunks,
                &left_raw,
                &self.in_left,
                self.ahead_rows,
                &*self.lookbacks,
            );

            workspace.calc_cells(
                left_tree,
                self.tree_rows,
                self.in_left.slice_rows,
                self.extend_mask2_left,
                self.ahead_rows,
                &*self.lookbacks,
                &self.lookups_left,
            );

            if self.odd_midline {
                workspace.calc_odd_midline_ahead(
                    left_tree,
                    self.tree_rows,
                    self.ahead_rows,
                    &*self.lookbacks,
                    &self.lookups_left,
                );
            };

            let right_tree = workspace.chunks_to_tree(
                &right_chunks,
                &right_raw,
                &self.in_right,
                self.ahead_rows,
                &*self.lookbacks,
            );

            workspace.calc_cells(
                right_tree,
                self.tree_rows,
                self.in_right.slice_rows,
                self.extend_mask2_right,
                self.ahead_rows,
                &*self.lookbacks,
                &self.lookups_right,
            );

            if workspace.large_guard[0].is_some() {
                // Free up some space now. Either left_raw or right_raw
                // is likely to remain empty to the end of this iteration.
                left_raw = Vec::new();
                right_raw = Vec::new();
            }

            // #4 Join trees
            if self.out_left.is_some() {
                workspace.join(
                    left_tree,
                    right_tree,
                    self.join_rows,
                    self.ahead_rows,
                    self.max_dist,
                    self.out_left.as_ref().unwrap().sort == self.in_right.sort && !self.partial,
                    &*self.lookbacks,
                    &self.lookups_left,
                    &self.lookups_right,
                );
            };

            if self.out_right.is_some() {
                workspace.join(
                    right_tree,
                    left_tree,
                    self.join_rows,
                    self.ahead_rows,
                    self.max_dist,
                    self.out_right.as_ref().unwrap().sort == self.in_left.sort && !self.partial,
                    &*self.lookbacks,
                    &self.lookups_right,
                    &self.lookups_left,
                );
            };

            // #5 (Optional) apply partial solution logic
            if self.partial {
                if self.out_left.is_some() {
                    workspace.best_partial(
                        left_tree,
                        self.out_left.as_ref().unwrap().slice_rows,
                        self.ahead_rows,
                    );
                    workspace.print_partial(
                        left_tree,
                        self.out_left.as_ref().unwrap().slice_rows,
                        self.ahead_rows,
                        &*self.lookbacks,
                    );
                };
                if self.out_right.is_some() {
                    workspace.best_partial(
                        right_tree,
                        self.out_right.as_ref().unwrap().slice_rows,
                        self.ahead_rows,
                    );
                    workspace.print_partial(
                        right_tree,
                        self.out_right.as_ref().unwrap().slice_rows,
                        self.ahead_rows,
                        &*self.lookbacks,
                    );
                };
            };

            // #6 (Optional) Exclude patterns we don't care about
            let mut exclude_pat = 0;
            while exclude_pat + 1 < self.exclude.len() {
                if self.out_left.is_some() {
                    workspace.exclude(
                        left_tree,
                        self.out_left.as_ref().unwrap().slice_rows,
                        self.ahead_rows,
                        &*self.lookbacks,
                        &self.exclude[exclude_pat],
                        &self.exclude[exclude_pat + 1],
                    );
                };
                if self.out_right.is_some() {
                    workspace.exclude(
                        right_tree,
                        self.out_right.as_ref().unwrap().slice_rows,
                        self.ahead_rows,
                        &*self.lookbacks,
                        &self.exclude[exclude_pat],
                        &self.exclude[exclude_pat + 1],
                    );
                };
                exclude_pat += 2;
            }

            // #7 Work out the most appropriate min_key_rows
            let max_key_rows = min(
                self.out_left
                    .as_ref()
                    .map_or(std::usize::MAX, |d| d.slice_rows),
                self.out_right
                    .as_ref()
                    .map_or(std::usize::MAX, |d| d.slice_rows),
            );

            let working_size = input_buf_size + workspace.size();
            let mut min_key_rows = chunk_match_rows
                + if working_size * 8 > self.workspace_large_threshold * 6 {
                    ((max_key_rows - chunk_match_rows) >> 1) - 1
                } else if working_size * 8 > self.workspace_large_threshold * 5 || self.split_chunks
                {
                    2
                } else if working_size * 8 > self.workspace_large_threshold * 4 {
                    1
                } else {
                    0
                };

            if min_key_rows + self.max_chunk_rows < max_key_rows {
                min_key_rows = max_key_rows - self.max_chunk_rows
            }
            if working_size * 8 > self.workspace_large_threshold * 10
                && min_key_rows + 8 < max_key_rows
            {
                min_key_rows = max_key_rows - 8;
            };
            if min_key_rows + max(self.ahead_rows, 4) > max_key_rows {
                // We can leave off one row because the root node also
                // has lookahead information.
                min_key_rows = max_key_rows + 1 - max(self.ahead_rows, 4);
            };

            // #8 Convert trees back into chunks

            left_chunks.clear();
            left_raw.clear();
            right_chunks.clear();
            right_raw.clear();

            if self.out_left.is_some() {
                slice_count_left = workspace.tree_to_chunks(
                    left_tree,
                    self.out_left.as_ref().unwrap(),
                    self.ahead_rows,
                    &mut left_chunks,
                    &mut left_raw,
                    &*self.lookbacks,
                    min_key_rows,
                );
            };
            if self.out_right.is_some() {
                slice_count_right = workspace.tree_to_chunks(
                    right_tree,
                    self.out_right.as_ref().unwrap(),
                    self.ahead_rows,
                    &mut right_chunks,
                    &mut right_raw,
                    &*self.lookbacks,
                    min_key_rows,
                );
            };

            // #9 Write out chunks to common buffers.
            // The buffers will be flushed when they get full.
            let mut io = self.out_io.lock().unwrap();

            {
                if !left_chunks.is_empty() {
                    io.left.as_mut().unwrap().extend(
                        &left_chunks,
                        &left_raw,
                        self.out_left.as_ref().unwrap(),
                        slice_count_left,
                    )?;
                };
                if !right_chunks.is_empty() {
                    io.right.as_mut().unwrap().extend(
                        &right_chunks,
                        &right_raw,
                        self.out_right.as_ref().unwrap(),
                        slice_count_right,
                    )?;
                };
            };

            if self.print_stats {
                println!("{:12}", workspace.size());
            };

            drop(io);

            if workspace.size() > 1 << 35 {
                eprintln!(
                    "Warning: {}% of max tree capacity used.",
                    workspace.size() / (0x10_0000_0000 / 100)
                );
            };

            if workspace.size() as isize > workspace.large_threshold[0] {
                // We used a lot of memory. Free it here.
                // We can end up here even if the semaphores were never taken
                // (calc_cells can add some additional space.)
                // Release the sem guards only after the old workspace is dropped.
                left_chunks = Vec::new();
                left_raw = Vec::new();
                right_chunks = Vec::new();
                right_raw = Vec::new();
                workspace.clear();
            };
        }

        Ok(())
    }

    fn flush_join(&self) -> io::Result<()> {
        let mut io = self.out_io.lock().unwrap();

        if let Some(ref mut out) = io.left {
            out.flush_out(self.out_left.as_ref().unwrap())?;
        };
        if let Some(ref mut out) = io.right {
            out.flush_out(self.out_right.as_ref().unwrap())?;
        };
        Ok(())
    }
}

struct Resize {
    in_io: Mutex<InMut>,
    out_io: Mutex<OutMut>,
    in_desc: SliceDesc,
    out_desc: SliceDesc,
    ahead_rows: usize,
    key_bytes: usize,
    tree_rows: usize,
    extend_mask2: u64,
    lookbacks: Box<[LvlLookBack]>,
    lookups_left: Lookups,
    threads: i32,
    workspace_large_threshold: usize,
    large_sem: [Semaphore; 4],
}

impl Resize {
    fn new(opt: &Matches) -> Result<Resize, Box<dyn error::Error + Send + Sync>> {
        let (period, starts, shifts) =
            workspace::parse_velocity(&opt.opt_str("velocity").unwrap())?;

        let (in_io, in_desc) = if opt.opt_present("in_left") {
            InMut::new(&opt.opt_str("in_left").unwrap(), SortSide::Left)?
        } else if opt.opt_present("in_right") {
            InMut::new(&opt.opt_str("in_right").unwrap(), SortSide::Right)?
        } else {
            return Err("in_left or in_right required".into());
        };

        let out_desc = SliceDesc::new(
            parse_opt(opt, "rows")?,
            SortSide::opt_parse(opt, "sort")?,
            if opt.opt_present("arithmetic") {
                Compression::Arithmetic
            } else if opt.opt_present("compress") {
                Compression::Rle
            } else {
                Compression::Packed
            },
        );

        let tree_rows = max(in_desc.slice_rows, out_desc.slice_rows);

        let mut extend_mask2 = opt
            .opt_str("extend_mask")
            .unwrap_or_else(|| "f".to_string())
            .chars()
            .next()
            .unwrap()
            .to_digit(16)
            .unwrap() as u64;

        extend_mask2 += extend_mask2 << 4;
        extend_mask2 += extend_mask2 << 8;
        extend_mask2 += extend_mask2 << 16;
        extend_mask2 += extend_mask2 << 32;

        let lookbacks = LvlLookBack::prepare(&*starts, &*shifts, tree_rows, period);

        let (lookups_left, _) =
            Lookups::create(&opt.opt_str("rule").unwrap_or_else(|| "B3/S23".to_string()));

        let flush_after = parse_opt_or(opt, "flush_after", 1 << 30)?;

        let out_io = OutMut::new(opt.opt_str("out").unwrap(), &out_desc, flush_after);

        let threads: i32 = parse_opt_or(opt, "threads", num_cpus::get() as i32)?;
        let mem: usize = parse_opt_or(opt, "mem", flush_after)?;
        let workspace_large_threshold: usize = min(
            mem / threads as usize,
            // If the workspace gets too large the internal indexes
            // will overflow u32. 0x2_0000_0000 provides a bit of margin
            // as the overflow won't occur until until past
            // 0x10_0000_0000.
            0x2_0000_0000,
        );
        let key_bytes = min(in_desc.key_bytes, out_desc.key_bytes);

        Ok(Resize {
            in_io: Mutex::new(in_io),
            out_io: Mutex::new(out_io),
            in_desc,
            out_desc,
            ahead_rows: starts.len(),
            key_bytes,
            tree_rows,
            extend_mask2,
            lookbacks,
            lookups_left,
            threads,
            workspace_large_threshold,
            large_sem: [
                Semaphore::new((threads + 1) as isize / 2),
                Semaphore::new((threads + 3) as isize / 4),
                Semaphore::new((threads + 7) as isize / 8),
                Semaphore::new((threads + 15) as isize / 16),
            ],
        })
    }

    fn do_work(&self) -> io::Result<()> {
        let mut workspace = Workspace::new(self.tree_rows, &self.large_sem);
        let mut chunks: Vec<usize> = Vec::new();
        let mut raw: Vec<u8> = Vec::new();

        let mut key_upper = vec![0u8; self.key_bytes].into_boxed_slice();

        loop {
            let mut chunk_rows = std::usize::MAX;

            chunks.clear();
            raw.clear();

            // #1 read in chunks
            let mut in_io = self.in_io.lock().unwrap();

            if in_io.eof() {
                break;
            };

            // make sure we read enough chunks to form full chunks when writing out.
            key_upper.copy_from_slice(&in_io.key_upper[0..self.key_bytes]);
            for i in min(self.in_desc.slice_rows, self.out_desc.slice_rows) - self.ahead_rows
                ..self.key_bytes * 8
            {
                key_upper[i >> 3] |= 0b1000_0000u8 >> (i & 7);
            }

            while !in_io.eof() && in_io.key_lower[0..self.key_bytes] <= *key_upper {
                chunk_rows = min(
                    chunk_rows,
                    self.in_desc.slice_rows - ((in_io.header()[4] & 127) as usize),
                );
                in_io.read_chunk(&self.in_desc, &mut chunks, &mut raw)?;
            }

            // #2 Drop mutex. After tis point we run in parallel.
            drop(in_io);

            // #3 Prepare tree
            workspace.clear();

            let input_buf_size = raw.len();
            workspace.large_threshold[0] =
                self.workspace_large_threshold as isize - input_buf_size as isize;
            workspace.large_threshold[1] =
                self.workspace_large_threshold as isize * 3 / 2 - input_buf_size as isize;
            workspace.large_threshold[2] =
                self.workspace_large_threshold as isize * 2 - input_buf_size as isize;
            workspace.large_threshold[3] =
                self.workspace_large_threshold as isize * 4 - input_buf_size as isize;

            let tree = workspace.chunks_to_tree(
                &chunks,
                &raw,
                &self.in_desc,
                self.ahead_rows,
                &self.lookbacks,
            );

            // #4 Use calc_cells to adjust the tree depth
            workspace.calc_cells(
                tree,
                self.tree_rows,
                self.in_desc.slice_rows,
                self.extend_mask2,
                self.ahead_rows,
                &self.lookbacks,
                &self.lookups_left,
            );

            // #5 Convert tree back into chunks
            chunks.clear();
            raw.clear();
            let slice_count = workspace.tree_to_chunks(
                tree,
                &self.out_desc,
                self.ahead_rows,
                &mut chunks,
                &mut raw,
                &self.lookbacks,
                if self.out_desc.slice_rows > self.in_desc.slice_rows {
                    // If Adding rows have minimal chunk size to avoid running out of memory when they
                    // are joined.
                    self.out_desc.slice_rows - self.ahead_rows
                } else {
                    min(chunk_rows, self.out_desc.slice_rows - self.ahead_rows)
                },
            );

            // #6 Write out chunks to common buffers.
            // The buffers will be flushed when they get full.
            let mut out_io = self.out_io.lock().unwrap();

            out_io.extend(&chunks, &raw, &self.out_desc, slice_count)?;

            drop(out_io);

            if (input_buf_size + workspace.size()) as isize > workspace.large_threshold[0]
                || workspace.large_guard[0].is_some()
            {
                // We used a lot of memory. Free it here.
                // We can end up here even if the semaphores were never taken
                // (calc_cells can add some additional space.)
                // The sem guards are released as the old workspace is dropped.
                chunks = Vec::new();
                raw = Vec::new();
                workspace = Workspace::new(self.tree_rows, &self.large_sem);
            };
        }

        Ok(())
    }

    fn flush_resize(&self) -> io::Result<()> {
        let mut out_io = self.out_io.lock().unwrap();

        out_io.flush_out(&self.out_desc)?;
        Ok(())
    }
}

fn dump_chunks(slice_in: &mut InMut, desc: &SliceDesc) -> io::Result<()> {
    let mut s = String::new();

    while !slice_in.eof() {
        {
            println!(
                "{:9} {:3}{}",
                u32::from_be_bytes(slice_in.header()[0..4].try_into().unwrap()),
                slice_in.header()[4] & 127,
                if slice_in.header()[4] & 128 != 0 {
                    "-"
                } else {
                    ""
                },
            );

            let key = &slice_in.header()[5..5 + desc.key_bytes];
            s.clear();
            for i in 0..desc.slice_rows - (slice_in.header()[4] & 127) as usize {
                s.push(if key[i >> 3] & (0x80u8 >> (i & 7)) != 0 {
                    '*'
                } else {
                    ' '
                });
            }
            s.push('|');
            println!("{}", s);

            let key = &slice_in.header()[5 + desc.key_bytes..5 + 2 * desc.key_bytes];
            s.clear();
            for i in 0..desc.slice_rows - (slice_in.header()[4] & 127) as usize {
                s.push(if key[i >> 3] & (0x80u8 >> (i & 7)) != 0 {
                    '*'
                } else {
                    ' '
                });
            }
            s.push('|');
            println!("{}", s);
        };
        slice_in.advance(desc)?;
    }

    Ok(())
}

fn display_stats(
    slice_in: &mut InMut,
    desc: &SliceDesc,
    ahead_rows: usize,
    lookbacks: &[LvlLookBack],
) -> io::Result<()> {
    let semaphores = [
        Semaphore::new(1),
        Semaphore::new(1),
        Semaphore::new(1),
        Semaphore::new(1),
    ];

    let mut workspace = Workspace::new(desc.slice_rows, &semaphores);

    let mut chunks: Vec<usize> = Vec::new();
    let mut raw: Vec<u8> = Vec::new();

    let mut counts = vec![0usize; ahead_rows].into_boxed_slice();
    let mut ahead_counts = vec![0usize; 4 * ahead_rows + 1].into_boxed_slice();
    let mut match_counts = vec![0usize; ahead_rows].into_boxed_slice();
    let mut previous_ahead2 = vec![0u64; 4 * ahead_rows].into_boxed_slice();

    while !slice_in.eof() {
        chunks.clear();
        raw.clear();
        slice_in.read_chunk(&desc, &mut chunks, &mut raw)?;

        workspace.clear();

        let tree = workspace.chunks_to_tree(&chunks, &raw, &desc, ahead_rows, &lookbacks);

        workspace.visit_tree(tree, desc.slice_rows, |lvl, child, ahead, ahead2, _dist| {
            if desc.slice_rows - lvl < ahead_rows {
                counts[desc.slice_rows - lvl] += 1;
                ahead_counts[4 * (desc.slice_rows - lvl) + ahead.count_ones() as usize] += 1;
                let previous_ahead2 =
                    &mut previous_ahead2[4 * (desc.slice_rows - lvl) + child as usize];
                if *previous_ahead2 == ahead2 {
                    match_counts[desc.slice_rows - lvl] += 1;
                } else {
                    *previous_ahead2 = ahead2;
                };
            }
        });
    }

    println!("Slice counts and ratios to last level count:");
    for lvl in 0..counts.len() {
        println!(
            "{:>12} {:4.1}",
            counts[lvl],
            counts[0] as f64 / counts[lvl] as f64
        );
    }

    println!();
    println!("Ahead proportions:");
    for lvl in 0..counts.len() {
        println!(
            " {:4.2} {:4.2} {:4.2} {:4.2} ({:3.1}) ahead2 unchanged: {:1.3}",
            ahead_counts[4 * lvl + 1] as f64 / counts[lvl] as f64,
            ahead_counts[4 * lvl + 2] as f64 / counts[lvl] as f64,
            ahead_counts[4 * lvl + 3] as f64 / counts[lvl] as f64,
            ahead_counts[4 * lvl + 4] as f64 / counts[lvl] as f64,
            (ahead_counts[4 * lvl + 1]
                + 2 * ahead_counts[4 * lvl + 2]
                + 3 * ahead_counts[4 * lvl + 3]
                + 4 * ahead_counts[4 * lvl + 4]) as f64
                / counts[lvl] as f64,
            match_counts[lvl] as f64 / counts[lvl] as f64,
        );
    }

    Ok(())
}

fn min_distance(
    slice_in: &mut InMut,
    desc: &SliceDesc,
    ahead_rows: usize,
    lookbacks: &[LvlLookBack],
) -> io::Result<u8> {
    let mut min_distance = 255u8;

    let semaphores = [
        Semaphore::new(1),
        Semaphore::new(1),
        Semaphore::new(1),
        Semaphore::new(1),
    ];

    let mut workspace = Workspace::new(desc.slice_rows, &semaphores);

    let mut chunks: Vec<usize> = Vec::new();
    let mut raw: Vec<u8> = Vec::new();

    while !slice_in.eof() {
        chunks.clear();
        raw.clear();
        slice_in.read_chunk(&desc, &mut chunks, &mut raw)?;

        workspace.clear();

        let tree = workspace.chunks_to_tree(&chunks, &raw, &desc, ahead_rows, &lookbacks);

        workspace.visit_tree(
            tree,
            desc.slice_rows,
            |lvl, _child, _ahead, _ahead2, dist| {
                if lvl == desc.slice_rows {
                    min_distance = min(min_distance, dist);
                };
            },
        );
    }

    Ok(min_distance)
}

fn slice_info(opt: &Matches) -> Result<(), Box<dyn error::Error + Send + Sync>> {
    let mut period = None;
    let mut starts = None;
    let mut shifts = Some(workspace::parse_shifts(
        &opt.opt_str("shifts").unwrap_or_else(|| "".to_string()),
    )?);

    if opt.opt_present("period") {
        period = Some(parse_opt(opt, "period")?);
    };

    if opt.opt_present("starts") {
        starts = Some(workspace::parse_starts(&opt.opt_str("starts").unwrap())?);
    };

    if opt.opt_present("velocity") {
        let (period_, starts_, shifts_) =
            workspace::parse_velocity(&opt.opt_str("velocity").unwrap())?;
        period = Some(period_);
        starts = Some(starts_);
        shifts = Some(shifts_);
    };

    if opt.opt_present("in_left") {
        let (mut in_left, desc) = InMut::new(&opt.opt_str("in_left").unwrap(), SortSide::Left)?;

        if opt.opt_present("slice_count") {
            println!("{}", in_left.slice_count());
        };
        if opt.opt_present("min_distance") {
            if period.is_some() && starts.is_some() {
                let lookbacks = LvlLookBack::prepare(
                    starts.as_ref().unwrap(),
                    shifts.as_ref().unwrap(),
                    desc.slice_rows,
                    period.unwrap(),
                );
                println!(
                    "{}",
                    min_distance(
                        &mut in_left,
                        &desc,
                        starts.as_ref().unwrap().len(),
                        &lookbacks,
                    )?
                );
            } else {
                eprintln!("Missing required configuration for --min distance");
            };
        }
        if opt.opt_present("chunks") {
            dump_chunks(&mut in_left, &desc)?;
        };
        if opt.opt_present("stats") {
            if period.is_some() && starts.is_some() {
                let lookbacks = LvlLookBack::prepare(
                    starts.as_ref().unwrap(),
                    shifts.as_ref().unwrap(),
                    desc.slice_rows,
                    period.unwrap(),
                );
                display_stats(
                    &mut in_left,
                    &desc,
                    starts.as_ref().unwrap().len(),
                    &lookbacks,
                )?;
            } else {
                eprintln!("Missing required configuration for --stats");
            };
        };
    };
    if opt.opt_present("in_right") {
        let (mut in_right, desc) = InMut::new(&opt.opt_str("in_right").unwrap(), SortSide::Right)?;

        if opt.opt_present("slice_count") {
            println!("{}", in_right.slice_count());
        };
        if opt.opt_present("min_distance") {
            if period.is_some() && starts.is_some() {
                let lookbacks = LvlLookBack::prepare(
                    starts.as_ref().unwrap(),
                    shifts.as_ref().unwrap(),
                    desc.slice_rows,
                    period.unwrap(),
                );
                println!(
                    "{}",
                    min_distance(
                        &mut in_right,
                        &desc,
                        starts.as_ref().unwrap().len(),
                        &lookbacks,
                    )?
                );
            } else {
                eprintln!("Missing required configuration for --min distance");
            };
        }
        if opt.opt_present("chunks") {
            dump_chunks(&mut in_right, &desc)?;
        };
        if opt.opt_present("stats") {
            if period.is_some() && starts.is_some() {
                let lookbacks = LvlLookBack::prepare(
                    starts.as_ref().unwrap(),
                    shifts.as_ref().unwrap(),
                    desc.slice_rows,
                    period.unwrap(),
                );
                display_stats(
                    &mut in_right,
                    &desc,
                    starts.as_ref().unwrap().len(),
                    &lookbacks,
                )?;
            } else {
                eprintln!("Missing required configuration for --stats");
            };
        };
    };

    if !opt.opt_present("in_left") && !opt.opt_present("in_right") && opt.opt_present("velocity") {
        println!("Equivalent legacy options:");
        println!("--period {}", period.unwrap());
        print!("--starts ");
        let mut first_start = true;
        for start in starts.unwrap().iter() {
            if first_start {
                print!("{},{}", start.gen, start.y);
                first_start = false;
            } else {
                print!(",{},{}", start.gen, start.y);
            };
        }
        println!();
        if shifts.as_ref().unwrap().len() > 0 {
            print!("--shifts ");
            let mut first_shift = true;
            for shift in shifts.unwrap().iter() {
                if first_shift {
                    print!("{}", shift);
                    first_shift = false;
                } else {
                    print!(",{}", shift);
                };
            }
            println!();
        }
    }
    Ok(())
}

fn print_usage(
    program: &str,
    init_opts: &Options,
    join_opts: &Options,
    slice_info_opts: &Options,
    resize_opts: &Options,
) {
    let brief = format!("Usage: {} init [options]", program);
    eprintln!("{}", init_opts.usage(&brief));
    let brief = format!("Usage: {} join [options]", program);
    eprintln!("{}", join_opts.usage(&brief));
    let brief = format!("Usage: {} resize [options]", program);
    eprintln!("{}", resize_opts.usage(&brief));
    let brief = format!("Usage: {} info [options]", program);
    eprintln!("{}", slice_info_opts.usage(&brief));
}

fn run() -> Result<(), Box<dyn error::Error + Send + Sync>> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let verb = args[1].clone();

    let mut init_opts = Options::new();
    init_opts.reqopt("", "seed", "Seed of slice in hex", "xxxx");
    init_opts.reqopt("", "slice_rows", "Number of bits in slice", "NUM");
    init_opts.optopt("", "period", "(deprecated) Period of the spaceship", "NUM");
    init_opts.optopt(
        "",
        "starts",
        "(deprecated) Start searching from",
        "gen,y,gen,y,gen,y...",
    );
    init_opts.optopt(
        "",
        "shifts",
        "(deprecated) Shift x at generations",
        "gen,gen...",
    );
    init_opts.optopt("", "velocity", "velocity", "(a,b)c/p");
    init_opts.optopt("", "rule", "Rule, B3/S23 for life", "B?/S?");
    init_opts.reqopt("", "sort", "sorting for left output", "left|right");
    init_opts.reqopt("", "out_dir", "output directory", "DIR");
    init_opts.optopt("", "max_chunk_rows", "Depth of chunks in rows", "NUM");
    init_opts.optflag("", "compress", "Compress slices");
    init_opts.optflag("", "arithmetic", "Use Arithmetic Compressor");

    let mut join_opts = Options::new();
    join_opts.optopt("", "period", "(deprecated) Period of the spaceship", "NUM");
    join_opts.optopt(
        "",
        "starts",
        "(deprecated) Start searching from",
        "gen,y,gen,y,gen,y...",
    );
    join_opts.optopt(
        "",
        "shifts",
        "(deprecated) Shift x at generations",
        "gen,gen...",
    );
    join_opts.optopt("", "velocity", "velocity", "(a,b)c/p");
    join_opts.optopt("", "rule", "Rule, B3/S23 for life", "B?/S?");
    join_opts.reqopt("", "in_left", "left input directory", "DIR");
    join_opts.reqopt("", "in_right", "right input directory", "DIR");
    join_opts.optopt("", "out_left", "left output directory", "DIR");
    join_opts.optopt("", "out_right", "right output directory", "DIR");
    join_opts.optopt("", "left_rows", "rows for left output", "NUM");
    join_opts.optopt("", "right_rows", "rows for right output", "NUM");
    join_opts.optopt("", "left_sort", "sorting for left output", "left|right");
    join_opts.optopt("", "right_sort", "sorting for right output", "left|right");
    join_opts.optopt(
        "",
        "flush_left_after",
        "Open new file after this number of bytes",
        "BYTES",
    );
    join_opts.optopt(
        "",
        "flush_right_after",
        "Open new file after this number of bytes",
        "BYTES",
    );
    join_opts.optopt("", "mem", "Working memory", "BYTES");
    join_opts.optopt("", "max_chunk_rows", "Depth of chunks in rows", "NUM");
    join_opts.optflag("", "even_midline", "midline of an even-width spaceship");
    join_opts.optflag("", "odd_midline", "midline of an odd-width spaceship");
    join_opts.optflag("", "gutter", "gutter of an odd-width spaceship");
    join_opts.optflag("", "print_stats", "print join statistics");
    join_opts.optflag("", "zero", "Zero extend slice");
    join_opts.optflag("", "nonzero", "Next row cannot be empty");
    join_opts.optopt("", "extend_mask", "Hex value to extend ship with", "x");
    join_opts.optflag("", "partial", "Pick slice for partial spaceship");
    join_opts.optflag("", "compress", "Compress slices");
    join_opts.optflag("", "arithmetic", "Use Arithmetic Compressor");
    join_opts.optopt("", "threads", "number of threads", "NUM");
    join_opts.optopt(
        "",
        "max_width",
        "Maximum width of the floating rows",
        "COLS",
    );
    join_opts.optopt(
        "",
        "exclude",
        "Exclude slices starting with pattern",
        "LEFT_COL/RIGHT_COL",
    );

    let mut slice_info_opts = Options::new();
    slice_info_opts.optopt("", "in_left", "left input directory", "DIR");
    slice_info_opts.optopt("", "in_right", "right input directory", "DIR");
    slice_info_opts.optopt("", "period", "Period of the spaceship", "NUM");
    slice_info_opts.optopt("", "starts", "Start searching from", "gen,y,gen,y,gen,y...");
    slice_info_opts.optopt("", "shifts", "Shift x at generations", "gen,gen...");
    slice_info_opts.optopt("", "velocity", "velocity", "(a,b)c/p");
    slice_info_opts.optflag("", "slice_count", "Count of Slices");
    slice_info_opts.optflag("", "min_distance", "Minimum distance to far edge");
    slice_info_opts.optflag("", "chunks", "Dump chunk headers");
    slice_info_opts.optflag("", "stats", "Calculate and print statistics");
    // The following are unused, but make using the program easier
    // by allowing parameters from the join subcommand be used with the
    // stats subcommand.
    slice_info_opts.optopt("", "rule", "Rule, B3/S23 for life (unused)", "B?/S?");
    slice_info_opts.optopt(
        "",
        "flush_left_after",
        "Open new file after this number of bytes (unused)",
        "BYTES",
    );
    slice_info_opts.optopt(
        "",
        "flush_right_after",
        "Open new file after this number of bytes (unused)",
        "BYTES",
    );
    slice_info_opts.optopt("", "mem", "Working memory (unused)", "BYTES");
    slice_info_opts.optopt(
        "",
        "max_chunk_rows",
        "Depth of chunks in rows (unused)",
        "NUM",
    );
    slice_info_opts.optflag("", "compress", "Compress slices (unused)");
    slice_info_opts.optflag("", "arithmetic", "Use Arithmetic Compressor (unused)");
    slice_info_opts.optopt("", "threads", "number of threads (unused)", "NUM");
    slice_info_opts.optopt(
        "",
        "max_width",
        "Maximum width of the floating rows (unused)",
        "COLS",
    );
    slice_info_opts.optopt(
        "",
        "exclude",
        "Exclude slices starting with pattern (unused)",
        "LEFT_COL/RIGHT_COL",
    );

    let mut resize_opts = Options::new();
    resize_opts.optopt("", "in_left", "left input directory", "DIR");
    resize_opts.optopt("", "in_right", "right input directory", "DIR");
    resize_opts.optopt("", "out", "output directory", "DIR");
    resize_opts.reqopt("", "rows", "rows for output", "NUM");
    resize_opts.optopt("", "sort", "output", "left|right");
    resize_opts.reqopt("", "velocity", "velocity", "(a,b)c/p");
    resize_opts.optopt(
        "",
        "flush_after",
        "Open new file after this number of bytes",
        "BYTES",
    );
    resize_opts.optflag("", "compress", "Compress slices (unused)");
    resize_opts.optflag("", "arithmetic", "Use Arithmetic Compressor (unused)");
    resize_opts.optopt("", "extend_mask", "Hex value to extend ship with", "x");
    // The following are unused, but make using the program easier
    // by allowing parameters from the join subcommand be used with the
    // resize subcommand.
    resize_opts.optopt("", "rule", "Rule, B3/S23 for life (unused)", "B?/S?");
    resize_opts.optopt("", "mem", "Working memory (unused)", "BYTES");
    resize_opts.optopt(
        "",
        "max_chunk_rows",
        "Depth of chunks in rows (unused)",
        "NUM",
    );
    resize_opts.optopt("", "threads", "number of threads (unused)", "NUM");
    resize_opts.optopt(
        "",
        "max_width",
        "Maximum width of the floating rows (unused)",
        "COLS",
    );
    resize_opts.optopt(
        "",
        "exclude",
        "Exclude slices starting with pattern (unused)",
        "LEFT_COL/RIGHT_COL",
    );

    match &verb[..] {
        "init" => {
            let opts = init_opts.parse(&args[2..])?;
            let init = Init::new(&opts)?;
            init.run()?;
        }
        "join" => {
            let opts = join_opts.parse(&args[2..])?;
            let join = Join::new(&opts)?;
            let join = Arc::new(join);
            let mut handles = Vec::new();

            for _ in 0..join.threads {
                let join = Arc::clone(&join);
                handles.push(thread::spawn(move || join.do_work()));
            }

            // join all the threads
            let results: Vec<_> = handles.into_iter().map(|x| x.join()).collect();

            // bubble errors and panics up.
            for result in results {
                result.unwrap()?;
            }

            join.flush_join()?;
        }
        "resize" => {
            let opts = resize_opts.parse(&args[2..])?;
            let resize = Resize::new(&opts)?;
            let resize = Arc::new(resize);
            let mut handles = Vec::new();

            for _ in 0..resize.threads {
                let resize = Arc::clone(&resize);
                handles.push(thread::spawn(move || resize.do_work()));
            }

            // join all the threads
            let results: Vec<_> = handles.into_iter().map(|x| x.join()).collect();

            // bubble errors and panics up.
            for result in results {
                result.unwrap()?;
            }

            resize.flush_resize()?;
        }
        "info" => {
            let opts = slice_info_opts.parse(&args[2..])?;
            slice_info(&opts)?;
        }
        "--version" => {
            println!("{}", env!("CARGO_PKG_VERSION"));
        }

        _ => {
            print_usage(
                &program,
                &init_opts,
                &join_opts,
                &slice_info_opts,
                &resize_opts,
            );
        }
    };
    Ok(())
}

fn main() {
    if let Err(err) = run() {
        eprintln!("error: {:?}", err);
        ::std::process::exit(1);
    }
}
