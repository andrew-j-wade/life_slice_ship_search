pub trait Compress {
    fn encode(&mut self, ctx: u8, c: u8);
    fn finish(&mut self);
}

pub trait Decompress {
    fn decode(&mut self, ctx: u8) -> u8;
    fn finish(&mut self);
}

pub struct PackedCompressor<'a> {
    have_partial: bool,
    partial: u8,
    output: &'a mut Vec<u8>,
}

impl<'a> PackedCompressor<'a> {
    pub fn new(output: &'a mut Vec<u8>) -> PackedCompressor<'a> {
        PackedCompressor {
            have_partial: false,
            partial: 0,
            output,
        }
    }
}

impl<'a> Compress for PackedCompressor<'a> {
    fn encode(&mut self, _ctx: u8, c: u8) {
        if self.have_partial {
            self.output.push(self.partial + c);
            self.have_partial = false;
        } else {
            self.partial = c << 4;
            self.have_partial = true;
        };
    }
    fn finish(&mut self) {
        if self.have_partial {
            self.output.push(self.partial);
        };
    }
}

pub struct PackedDecompressor<'a> {
    pos: usize,
    input: &'a [u8],
}

impl<'a> PackedDecompressor<'a> {
    pub fn new(input: &'a [u8]) -> PackedDecompressor<'a> {
        PackedDecompressor { pos: 0, input }
    }
}

impl<'a> Decompress for PackedDecompressor<'a> {
    fn decode(&mut self, _ctx: u8) -> u8 {
        let rv = if self.pos & 1 == 0 {
            self.input[self.pos >> 1] >> 4
        } else {
            self.input[self.pos >> 1] & 0x0f
        };
        self.pos += 1;
        rv
    }

    fn finish(&mut self) {
        assert_eq!(self.input.len(), (self.pos + 1) >> 1);
    }
}

// Arithmetic coder based on
// http://marknelson.us/2014/10/19/data-compression-with-arithmetic-coding/

struct BitWriter<'a> {
    partial: u8,
    partial_bits: u32,
    bits_pending: u32,
    output: &'a mut Vec<u8>,
}

impl<'a> BitWriter<'a> {
    pub fn new(output: &'a mut Vec<u8>) -> BitWriter<'a> {
        BitWriter {
            partial: 0,
            partial_bits: 0,
            bits_pending: 0,
            output,
        }
    }

    fn output_set_bit(&mut self) {
        self.partial |= 1;
        if self.partial_bits < 7 {
            self.partial_bits += 1;
            self.partial <<= 1;
        } else {
            self.output.push(self.partial);
            self.partial = 0;
            self.partial_bits = 0;
        };

        while self.bits_pending > 0 {
            if self.partial_bits < 7 {
                self.partial_bits += 1;
                self.partial <<= 1;
            } else {
                self.output.push(self.partial);
                self.partial = 0;
                self.partial_bits = 0;
            };
            self.bits_pending -= 1;
        }
    }

    fn output_clear_bit(&mut self) {
        if self.partial_bits < 7 {
            self.partial_bits += 1;
            self.partial <<= 1;
        } else {
            self.output.push(self.partial);
            self.partial = 0;
            self.partial_bits = 0;
        };

        while self.bits_pending > 0 {
            self.partial |= 1;
            if self.partial_bits < 7 {
                self.partial_bits += 1;
                self.partial <<= 1;
            } else {
                self.output.push(self.partial);
                self.partial = 0;
                self.partial_bits = 0;
            };
            self.bits_pending -= 1;
        }
    }

    fn add_pending_bit(&mut self) {
        self.bits_pending += 1;
    }
}

fn update_freq(freq: &mut [u32; 17], c: usize) {
    let mut old_freq = 0u32;
    let mut new_freq = 0u32;
    let update = c + 1;

    #[allow(clippy::needless_range_loop)]
    for i in 1..17 {
        let next_freq = freq[i];
        new_freq += ((next_freq - old_freq) * 7 + 8) >> 3;
        if update == i {
            new_freq += 256;
        };
        freq[i] = new_freq;
        old_freq = next_freq;
    }
}

pub struct ArithmeticCompressor<'a> {
    bitwriter: BitWriter<'a>,
    low: u32,
    high: u32,
    cumulative_freq: [[u32; 17]; 67],
}

impl<'a> ArithmeticCompressor<'a> {
    pub fn new(output: &'a mut Vec<u8>) -> ArithmeticCompressor<'a> {
        ArithmeticCompressor {
            bitwriter: BitWriter::new(output),
            low: 0b0000_0000_0000_0000,
            high: 0b1111_1111_1111_1111,
            cumulative_freq: [
                // masks where both 01 and 10 are allowed, or neither are
                // allowed, are more frequent.
                // This isn't the right distribution for encoding edge distance
                // (contexts 64, 65, 66), but those will adapt quickly.
                [0x000,
                 0x020, 0x040, 0x048, 0x050, 0x058, 0x060, 0x080, 0x0a0,
                 0x0c0, 0x0e0, 0x0e8, 0x0f0, 0x0f8, 0x100, 0x120, 0x140]; 67
            ],
        }
    }
}

impl<'a> Compress for ArithmeticCompressor<'a> {
    fn encode(&mut self, ctx: u8, c: u8) {
        let ctx = ctx as usize;
        let c = c as usize;
        let cumulative_freq = &mut self.cumulative_freq[ctx];
        let count = cumulative_freq[16];
        let range = self.high - self.low + 1;
        self.high = self.low + range * cumulative_freq[c + 1] / count - 1;
        self.low += range * cumulative_freq[c] / count;

        loop {
            if self.high < 0b1000_0000_0000_0000 {
                self.bitwriter.output_clear_bit();
            } else if self.low >= 0b1000_0000_0000_0000 {
                self.bitwriter.output_set_bit();
            } else if self.low >= 0b0100_0000_0000_0000 && self.high < 0b1100_0000_0000_0000 {
                self.bitwriter.add_pending_bit();
                self.low -= 0b0100_0000_0000_0000;
                self.high -= 0b0100_0000_0000_0000;
            } else {
                break;
            };
            self.high <<= 1;
            self.high += 1;
            self.low <<= 1;
            self.high &= 0b1111_1111_1111_1111;
            self.low &= 0b1111_1111_1111_1111;
        }

        // update model
        update_freq(cumulative_freq, c);
    }

    fn finish(&mut self) {
        for _ in 0..24 {
            if self.low >= 0b1000_0000_0000_0000 {
                self.bitwriter.output_set_bit();
            } else {
                self.bitwriter.output_clear_bit();
            };
            self.low <<= 1;
            self.low &= 0b1111_1111_1111_1111;
        }
    }
}

pub struct ArithmeticDecompressor<'a> {
    pos: usize,
    input: &'a [u8],
    low: u32,
    high: u32,
    value: u32,
    cumulative_freq: [[u32; 17]; 67],
}

impl<'a> ArithmeticDecompressor<'a> {
    pub fn new(input: &'a [u8]) -> ArithmeticDecompressor<'a> {
        ArithmeticDecompressor {
            pos: 16,
            input,
            low: 0b0000_0000_0000_0000,
            high: 0b1111_1111_1111_1111,
            value: ((input[0] as u32) << 8) + input[1] as u32,
            cumulative_freq: [[
                0x000, 0x020, 0x040, 0x048, 0x050, 0x058, 0x060, 0x080, 0x0a0, 0x0c0, 0x0e0, 0x0e8,
                0x0f0, 0x0f8, 0x100, 0x120, 0x140,
            ]; 67],
        }
    }
}

impl<'a> Decompress for ArithmeticDecompressor<'a> {
    fn decode(&mut self, ctx: u8) -> u8 {
        let ctx = ctx as usize;
        let cumulative_freq = &mut self.cumulative_freq[ctx];
        let count = cumulative_freq[16];
        let range = self.high - self.low + 1;
        let scaled_value = ((self.value - self.low + 1) * cumulative_freq[16] - 1) / range;

        let mut c = 15usize;
        for i in (1..16).rev() {
            if scaled_value < cumulative_freq[i] {
                c = i as usize - 1
            };
        }

        self.high = self.low + range * cumulative_freq[c + 1] / count - 1;
        self.low += range * cumulative_freq[c] / count;

        loop {
            if self.high < 0b1000_0000_0000_0000 {
                // do nothing
            } else if self.low >= 0b1000_0000_0000_0000 {
                self.value -= 0b1000_0000_0000_0000;
                self.high -= 0b1000_0000_0000_0000;
                self.low -= 0b1000_0000_0000_0000;
            } else if self.low >= 0b0100_0000_0000_0000 && self.high < 0b1100_0000_0000_0000 {
                self.value -= 0b0100_0000_0000_0000;
                self.high -= 0b0100_0000_0000_0000;
                self.low -= 0b0100_0000_0000_0000;
            } else {
                break;
            };
            self.high <<= 1;
            self.high += 1;
            self.low <<= 1;
            self.value <<= 1;
            self.value += (self.input[self.pos >> 3] >> (7 - (self.pos & 7))) as u32 & 1;
            self.pos += 1;
        }

        // update model
        update_freq(cumulative_freq, c);

        c as u8
    }

    fn finish(&mut self) {
        assert_eq!(self.low, self.value);
    }
}

#[derive(Copy, Clone)]
#[repr(align(4))]
struct RleState {
    run: u8,
    next_run: u8,
    runs_left: u8,
}

const RLE_ENCODING: [RleState; 16] = [
    RleState {
        run: 1,
        next_run: 1,
        runs_left: 2,
    },
    RleState {
        run: 1,
        next_run: 2,
        runs_left: 2,
    },
    RleState {
        run: 1,
        next_run: 3,
        runs_left: 2,
    },
    RleState {
        run: 1,
        next_run: 3,
        runs_left: 1,
    },
    RleState {
        run: 2,
        next_run: 1,
        runs_left: 2,
    },
    RleState {
        run: 2,
        next_run: 2,
        runs_left: 2,
    },
    RleState {
        run: 2,
        next_run: 3,
        runs_left: 2,
    },
    RleState {
        run: 2,
        next_run: 3,
        runs_left: 1,
    },
    RleState {
        run: 3,
        next_run: 1,
        runs_left: 2,
    },
    RleState {
        run: 3,
        next_run: 2,
        runs_left: 2,
    },
    RleState {
        run: 3,
        next_run: 2,
        runs_left: 1,
    },
    RleState {
        run: 4,
        next_run: 0,
        runs_left: 1,
    },
    RleState {
        run: 5,
        next_run: 0,
        runs_left: 1,
    },
    RleState {
        run: 6,
        next_run: 0,
        runs_left: 1,
    },
    RleState {
        run: 6,
        next_run: 0,
        runs_left: 0,
    },
    RleState {
        run: 16,
        next_run: 0,
        runs_left: 0,
    },
];

const fn create_lookup() -> [[u8; 64]; 64] {
    let mut table = [[0; 64]; 64];

    let mut run = 0;
    while run < 64 {
        let mut next_run = 0;
        while next_run < 64 {
            let mut encoding = 0;
            while encoding < 15 {
                encoding += 1;
                let state = RLE_ENCODING[encoding as usize];
                if run < state.run {
                    continue;
                }
                if run == state.run && state.runs_left == 0 {
                    continue;
                }
                if next_run < state.next_run {
                    continue;
                }
                if next_run == state.next_run && state.runs_left == 1 {
                    continue;
                }
                table[run as usize][next_run as usize] = encoding
            }
            next_run += 1
        }
        run += 1
    }

    table
}

static ENCODING_LOOKUP: [[u8; 64]; 64] = create_lookup();

pub struct RleCompressor<'a> {
    inner: PackedCompressor<'a>,
    state: RleState,
    predictor: [u8; 256],
    literal: u32,
    buffer: u64,
}

impl<'a> RleCompressor<'a> {
    pub fn new(output: &'a mut Vec<u8>) -> RleCompressor<'a> {
        //because run = 16 and literal = 0 nothing is output for the first 16 encode calls
        //allowing the buffer to fill. The predictor being 255 for all entries means the first
        //run will be a literal run to match what the decompressr expects.
        RleCompressor {
            inner: PackedCompressor::new(output),
            state: RleState {
                run: 16,
                next_run: 0,
                runs_left: 0,
            },
            predictor: [255; 256],
            literal: 0,
            buffer: 0,
        }
    }
}

impl<'a> Compress for RleCompressor<'a> {
    fn encode(&mut self, ctx: u8, c: u8) {
        let mut rle_encoding_needed = false;

        self.literal = if c == self.predictor[ctx as usize] {
            self.literal
        } else {
            self.literal + (1 << 16)
        };
        if self.state.run == 0 {
            if self.state.runs_left > 0 {
                self.state.run = self.state.next_run;
                self.state.next_run = 0;
                self.state.runs_left -= 1;
                if self.state.run == 0 {
                    rle_encoding_needed = true;
                };
            } else {
                rle_encoding_needed = true;
            };
        }
        if rle_encoding_needed {
            let normalized_literal = if self.literal & 1 == 1 {
                self.literal
            } else {
                !self.literal
            };
            let run = normalized_literal.trailing_ones();
            let next_run = (normalized_literal >> run).trailing_zeros();
            let rle_encoding = ENCODING_LOOKUP[run as usize][next_run as usize];
            self.state = RLE_ENCODING[rle_encoding as usize];
            self.inner.encode(0, rle_encoding);
        }
        if self.literal & 1 == 1 {
            self.inner.encode(0, (self.buffer & 0xF) as u8)
        };
        self.state.run -= 1;
        self.buffer >>= 4;
        self.literal >>= 1;
        self.predictor[ctx as usize] = c;
        self.buffer += (c as u64) << 60;
    }

    fn finish(&mut self) {
        //flush buffer
        for _ in 0..16 {
            self.encode(self.predictor[0], 0);
        }
        self.inner.finish();
    }
}

pub struct RleDecompressor<'a> {
    inner: PackedDecompressor<'a>,
    literal_run: bool,
    state: RleState,
    predictor: [u8; 256],
}

impl<'a> RleDecompressor<'a> {
    pub fn new(input: &'a [u8]) -> RleDecompressor<'a> {
        RleDecompressor {
            inner: PackedDecompressor::new(input),
            literal_run: true,
            state: RleState {
                run: 0,
                next_run: 0,
                runs_left: 0,
            },
            predictor: [255; 256],
        }
    }
}

impl<'a> Decompress for RleDecompressor<'a> {
    fn decode(&mut self, ctx: u8) -> u8 {
        if self.state.run == 0 {
            if self.state.runs_left > 0 {
                self.literal_run = !self.literal_run;
                self.state.run = self.state.next_run;
                self.state.next_run = 0;
                self.state.runs_left -= 1;
                if self.state.run == 0 {
                    self.state = RLE_ENCODING[self.inner.decode(0) as usize];
                };
            } else {
                self.state = RLE_ENCODING[self.inner.decode(0) as usize];
            };
        };
        self.state.run -= 1;
        if self.literal_run {
            self.predictor[ctx as usize] = self.inner.decode(0);
        };
        self.predictor[ctx as usize]
    }

    fn finish(&mut self) {
        self.inner.finish();
    }
}
