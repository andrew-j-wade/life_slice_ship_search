use std::convert::TryInto;

use getopts::Matches;

use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::BufWriter;

use std::cmp::min;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum SortSide {
    Left,
    Right,
}

impl SortSide {
    pub fn opt_parse(matches: &Matches, opt: &str) -> Result<SortSide, String> {
        match &matches.opt_str(opt).unwrap()[..] {
            "left" => Ok(SortSide::Left),
            "right" => Ok(SortSide::Right),
            s => Err(format!("Unexpected sort for {}: {}", opt, s)),
        }
    }
}

impl Default for SortSide {
    fn default() -> SortSide {
        SortSide::Left
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Compression {
    Packed,
    Arithmetic,
    Rle,
}

impl Default for Compression {
    fn default() -> Compression {
        Compression::Rle
    }
}

//global per fileset static data
#[derive(Clone, PartialEq, Eq, Debug, Default)]
pub struct SliceDesc {
    pub slice_rows: usize,
    pub key_start: usize,
    pub key_bytes: usize,
    pub sort: SortSide,
    pub compression: Compression,
    pub chunk_head_size: usize,
}

impl SliceDesc {
    pub fn new(slice_rows: usize, sort: SortSide, compression: Compression) -> SliceDesc {
        let key_bytes = (slice_rows + 7) >> 3;
        SliceDesc {
            slice_rows,
            key_start: match sort {
                SortSide::Left => key_bytes + 5,
                SortSide::Right => 5,
            },
            key_bytes,
            sort,
            compression,
            chunk_head_size: 2 * key_bytes + 5,
        }
    }
}

// global mutable data

struct InFile {
    header: Vec<u8>,
    chunks_remaining: u64,
    current_offset: u64,
    reader_offset: u64,
    header_file: BufReader<File>,
    data_file: BufReader<File>,
    data_filename: String,
    slice_count: u64,
}

impl InFile {
    fn new(header_filename: String, data_filename: String) -> io::Result<(InFile, SliceDesc)> {
        let mut header_file = BufReader::new(File::open(&header_filename)?);
        let mut header = Vec::with_capacity(32);
        let mut desc = SliceDesc::default();

        let bytesread = io::copy(&mut (&mut header_file).take(32), &mut header)?;

        if bytesread < 32 || !header.starts_with(&[b'S', b'L', b'I', b'3']) {
            return Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                format!("Invalid Header in {}", &header_filename),
            ));
        };
        let flags = u32::from_be_bytes(header[4..8].try_into().unwrap());
        if flags & 0xFFFF_FFF8 != 0 || flags & 6 == 6 {
            return Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                format!("Unsupported Format for {}", &header_filename),
            ));
        };

        desc.sort = if flags & 1 != 0 {
            SortSide::Left
        } else {
            SortSide::Right
        };
        desc.compression = if flags & 2 != 0 {
            Compression::Arithmetic
        } else if flags & 4 != 0 {
            Compression::Rle
        } else {
            Compression::Packed
        };
        desc.slice_rows = u32::from_be_bytes(header[8..12].try_into().unwrap()) as usize;
        desc.key_bytes = (desc.slice_rows + 7) >> 3;
        desc.key_start = match desc.sort {
            SortSide::Left => desc.key_bytes + 5,
            SortSide::Right => 5,
        };
        desc.chunk_head_size = 2 * desc.key_bytes + 5;
        let chunks_remaining = u64::from_be_bytes(header[16..24].try_into().unwrap());
        let slice_count = u64::from_be_bytes(header[24..32].try_into().unwrap());

        let data_file = BufReader::new(File::open(&data_filename).unwrap());

        header.clear();
        header.reserve_exact(desc.chunk_head_size);
        header.resize(desc.chunk_head_size, 0u8); // dummy chunk for first
                                                  // chunk to delta against.

        let mut infile = InFile {
            header,
            chunks_remaining: chunks_remaining + 1, // include dummy chunk
            current_offset: 0,
            reader_offset: 0,
            header_file,
            data_file,
            data_filename,
            slice_count,
        };

        infile.advance(&desc)?;

        Ok((infile, desc))
    }

    fn eof(&self) -> bool {
        self.chunks_remaining == 0
    }

    fn key(&self, desc: &SliceDesc) -> &[u8] {
        &self.header[desc.key_start..desc.key_start + desc.key_bytes]
    }

    fn advance(&mut self, desc: &SliceDesc) -> io::Result<()> {
        let adjust = u32::from_be_bytes(self.header[..4].try_into().unwrap());
        self.current_offset += adjust as u64;
        self.chunks_remaining -= 1;
        if self.chunks_remaining > 0 {
            let mut tail_bytes = [0u8; 2];
            self.header_file.read_exact(&mut tail_bytes)?;
            self.header_file.read_exact(&mut self.header[0..5])?;
            let key_chunk_bytes = (desc.slice_rows + 7 - (self.header[4] & 127) as usize) >> 3;

            if tail_bytes[0] == 0xff {
                self.header_file
                    .read_exact(&mut self.header[5..5 + key_chunk_bytes])?;
            } else if tail_bytes[0] > 0 {
                self.header_file.read_exact(
                    &mut self.header
                        [5 + key_chunk_bytes - tail_bytes[0] as usize..5 + key_chunk_bytes],
                )?;
            };
            if tail_bytes[1] == 0xff {
                self.header_file.read_exact(
                    &mut self.header[5 + desc.key_bytes..5 + desc.key_bytes + key_chunk_bytes],
                )?;
            } else if tail_bytes[1] > 0 {
                self.header_file.read_exact(
                    &mut self.header[5 + desc.key_bytes + key_chunk_bytes - tail_bytes[1] as usize
                        ..5 + desc.key_bytes + key_chunk_bytes],
                )?;
            };
        };

        Ok(())
    }

    fn read_slice(&mut self, chunks: &mut Vec<usize>, raw: &mut Vec<u8>) -> io::Result<()> {
        chunks.push(raw.len());
        raw.extend_from_slice(&self.header[..]);

        if self.current_offset != self.reader_offset {
            self.data_file
                .seek_relative(self.current_offset as i64 - self.reader_offset as i64)?;
            self.reader_offset = self.current_offset;
        };

        let bytes_to_read = u32::from_be_bytes(self.header[..4].try_into().unwrap()) as u64;

        let bytes_read = io::copy(&mut (&mut self.data_file).take(bytes_to_read), raw)?;

        if bytes_read == bytes_to_read {
            self.reader_offset += bytes_read;
            Ok(())
        } else {
            Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                format!(
                    "Could not read slice in {} at offset {} bytes {}",
                    &self.data_filename, &self.reader_offset, &bytes_to_read
                ),
            ))
        }
    }
}

pub struct InMut {
    pub key_lower: Box<[u8]>,
    pub key_upper: Box<[u8]>,
    chunk_rows: usize,

    //We may have a lot of swaps in the heap so the extra indirection of the box
    //is probably a win.
    #[allow(clippy::vec_box)]
    heap: Vec<Box<InFile>>,
}

impl InMut {
    pub fn new(dir: &str, sort: SortSide) -> io::Result<(InMut, SliceDesc)> {
        let mut heap: Vec<Box<InFile>> = Vec::new();
        let filenm = format!(
            "{}/{}_{:>03}.slice",
            dir,
            match sort {
                SortSide::Left => "l",
                SortSide::Right => "r",
            },
            0
        );
        let data_filenm = format!(
            "{}/{}_{:>03}.branches.slice",
            dir,
            match sort {
                SortSide::Left => "l",
                SortSide::Right => "r",
            },
            0
        );

        let (infile, desc) = InFile::new(filenm, data_filenm)?;

        assert_eq!(sort, desc.sort);

        heap.push(Box::new(infile));

        for i in 1.. {
            let filenm = format!(
                "{}/{}_{:>03}.slice",
                dir,
                match sort {
                    SortSide::Left => "l",
                    SortSide::Right => "r",
                },
                i
            );
            let data_filenm = format!(
                "{}/{}_{:>03}.branches.slice",
                dir,
                match sort {
                    SortSide::Left => "l",
                    SortSide::Right => "r",
                },
                i
            );

            match InFile::new(filenm, data_filenm) {
                Ok((infile, next_desc)) => {
                    assert_eq!(desc, next_desc);
                    heap.push(Box::new(infile));
                }
                Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
                    break;
                }
                Err(e) => {
                    return Err(e);
                }
            };
        }

        //heapify
        let mut n = heap.len();
        while n > 0 {
            n -= 1;
            let mut pos = n;
            let mut child = 2 * pos + 1;
            while child < heap.len() {
                let right = child + 1;
                if right < heap.len() && *heap[right].key(&desc) < *heap[child].key(&desc) {
                    child = right;
                };
                if *heap[child].key(&desc) < *heap[pos].key(&desc) {
                    heap.swap(pos, child);
                } else {
                    break;
                };
                pos = child;
                child = 2 * pos + 1;
            }
        }

        let mut key_lower = Box::<[u8]>::from(heap[0].key(&desc));
        let mut key_upper = Box::<[u8]>::from(&key_lower[..]);
        let chunk_rows = (heap[0].header[4] & 127) as usize;

        for i in desc.slice_rows - chunk_rows..key_lower.len() * 8 {
            key_lower[i >> 3] &= !(0b1000_0000u8 >> (i & 7));
            key_upper[i >> 3] |= 0b1000_0000u8 >> (i & 7);
        }

        Ok((
            InMut {
                key_lower,
                key_upper,
                chunk_rows,
                heap,
            },
            desc,
        ))
    }

    pub fn eof(&self) -> bool {
        self.heap.is_empty()
    }

    fn sift(&mut self, desc: &SliceDesc) {
        let mut pos: usize = 0;
        let mut child: usize = 1;
        while child < self.heap.len() {
            let right = child + 1;
            if right < self.heap.len() && *self.heap[right].key(desc) < *self.heap[child].key(desc)
            {
                child = right;
            };
            if *self.heap[child].key(desc) < *self.heap[pos].key(desc) {
                self.heap.swap(pos, child);
            } else {
                break;
            };
            pos = child;
            child = 2 * pos + 1;
        }
    }

    pub fn advance(&mut self, desc: &SliceDesc) -> io::Result<()> {
        self.heap[0].advance(desc)?;
        if self.heap[0].eof() {
            self.heap.swap_remove(0);
        };
        //sift
        self.sift(desc);

        if !self.eof() {
            self.chunk_rows = (self.heap[0].header[4] & 127) as usize;
            self.key_lower.copy_from_slice(self.heap[0].key(desc));
            self.key_upper.copy_from_slice(&self.key_lower);

            for i in desc.slice_rows - self.chunk_rows..self.key_lower.len() * 8 {
                self.key_lower[i >> 3] &= !(0b1000_0000u8 >> (i & 7));
                self.key_upper[i >> 3] |= 0b1000_0000u8 >> (i & 7);
            }
        };

        Ok(())
    }

    pub fn read_chunk(
        &mut self,
        desc: &SliceDesc,
        chunks: &mut Vec<usize>,
        raw: &mut Vec<u8>,
    ) -> io::Result<()> {
        self.heap[0].read_slice(chunks, raw)?;
        self.advance(desc)
    }

    pub fn header(&self) -> &[u8] {
        &self.heap[0].header
    }

    pub fn slice_count(&self) -> u64 {
        (&self.heap).iter().map(|in_mut| in_mut.slice_count).sum()
    }

    pub fn chunks_remaining(&self) -> u64 {
        (&self.heap)
            .iter()
            .map(|in_mut| in_mut.chunks_remaining)
            .sum()
    }
}

pub struct OutMut {
    raw: Vec<u8>,
    chunks: Vec<usize>,
    header: Box<[u8]>,
    pub flush_after: usize,
    dir: String,
    file_num: i32,
    slice_count: u64,
}

impl OutMut {
    pub fn new(dir: String, desc: &SliceDesc, flush_after: usize) -> OutMut {
        OutMut {
            raw: Vec::with_capacity(flush_after),
            chunks: Vec::new(),
            header: vec![0u8; desc.chunk_head_size].into_boxed_slice(),
            flush_after,
            dir,
            file_num: 0,
            slice_count: 0,
        }
    }

    pub fn flush_out(&mut self, desc: &SliceDesc) -> io::Result<()> {
        let raw = &mut self.raw;
        let chunks = &mut self.chunks;
        if chunks.is_empty() {
            return Ok(());
        };
        chunks.sort_by(|&a, &b| {
            raw[a + desc.key_start..a + desc.key_start + desc.key_bytes]
                .cmp(&raw[b + desc.key_start..b + desc.key_start + desc.key_bytes])
        });

        let filenm = format!(
            "{}/{}_{:>03}.slice",
            &self.dir,
            match desc.sort {
                SortSide::Left => "l",
                SortSide::Right => "r",
            },
            self.file_num
        );

        let mut file = BufWriter::new(File::create(filenm)?);

        file.write_all(&[b'S', b'L', b'I', b'3'])?;
        file.write_all(
            &(match desc.sort {
                SortSide::Left => 1u32,
                SortSide::Right => 0u32,
            } + match desc.compression {
                Compression::Packed => 0u32,
                Compression::Arithmetic => 2u32,
                Compression::Rle => 4u32,
            })
            .to_be_bytes(),
        )?;
        file.write_all(&(desc.slice_rows as u32).to_be_bytes())?;
        file.write_all(&[0u8; 4])?; // filler
        file.write_all(&(chunks.len() as u64).to_be_bytes())?;
        file.write_all(&(self.slice_count as u64).to_be_bytes())?;

        self.header.iter_mut().for_each(|x| *x = 0);
        let mut last_slice_key_bytes = 0;
        for &chunk in &*chunks {
            {
                let slice_key_bytes = (desc.slice_rows - (raw[chunk + 4] & 127) as usize + 7) >> 3;

                // The compression and decompression buffers won't be in
                // sync past the last slice_key_bytes so it is important
                // not to match on those bytes.
                let match_limit = min(last_slice_key_bytes, slice_key_bytes);
                last_slice_key_bytes = slice_key_bytes;

                let old_left = &self.header[5..5 + desc.key_bytes];
                let new_left = &raw[chunk + 5..chunk + 5 + desc.key_bytes];
                let mut left_matching_bytes = 0usize;
                while left_matching_bytes < match_limit {
                    if old_left[left_matching_bytes] != new_left[left_matching_bytes] {
                        break;
                    };
                    left_matching_bytes += 1;
                }
                let old_right = &self.header[5 + desc.key_bytes..5 + 2 * desc.key_bytes];
                let new_right = &raw[chunk + 5 + desc.key_bytes..chunk + 5 + 2 * desc.key_bytes];
                let mut right_matching_bytes = 0usize;
                while right_matching_bytes < match_limit {
                    if old_right[right_matching_bytes] != new_right[right_matching_bytes] {
                        break;
                    };
                    right_matching_bytes += 1;
                }

                let left_bytes;
                let right_bytes;

                if left_matching_bytes >= slice_key_bytes {
                    file.write_all(&[0u8])?;
                    left_bytes = &new_left[slice_key_bytes..slice_key_bytes];
                } else if left_matching_bytes == 0 || slice_key_bytes - left_matching_bytes > 254 {
                    file.write_all(&[255u8])?;
                    left_bytes = &new_left[..slice_key_bytes];
                } else {
                    file.write_all(&[(slice_key_bytes - left_matching_bytes) as u8])?;
                    left_bytes = &new_left[left_matching_bytes..slice_key_bytes];
                };

                if right_matching_bytes >= slice_key_bytes {
                    file.write_all(&[0u8])?;
                    right_bytes = &new_right[slice_key_bytes..slice_key_bytes];
                } else if right_matching_bytes == 0 || slice_key_bytes - right_matching_bytes > 254
                {
                    file.write_all(&[255u8])?;
                    right_bytes = &new_right[..slice_key_bytes];
                } else {
                    file.write_all(&[(slice_key_bytes - right_matching_bytes) as u8])?;
                    right_bytes = &new_right[right_matching_bytes..slice_key_bytes];
                };

                file.write_all(&raw[chunk..chunk + 5])?;
                file.write_all(left_bytes)?;
                file.write_all(right_bytes)?;
            }

            self.header
                .copy_from_slice(&raw[chunk..chunk + desc.chunk_head_size]);
        }
        file.flush()?;

        let filenm = format!(
            "{}/{}_{:>03}.branches.slice",
            &self.dir,
            match desc.sort {
                SortSide::Left => "l",
                SortSide::Right => "r",
            },
            self.file_num
        );

        let mut file = BufWriter::new(File::create(filenm)?);

        for &chunk in &*chunks {
            file.write_all(
                &raw[chunk + desc.chunk_head_size
                    ..chunk
                        + desc.chunk_head_size
                        + u32::from_be_bytes(raw[chunk..chunk + 4].try_into().unwrap()) as usize],
            )?;
        }
        file.flush()?;

        self.file_num += 1;

        raw.clear();
        chunks.clear();
        self.slice_count = 0;

        Ok(())
    }

    pub fn extend(
        &mut self,
        chunks: &[usize],
        raw: &[u8],
        desc: &SliceDesc,
        slice_count: u64,
    ) -> io::Result<()> {
        // If we flush half way through some of the slices attributed
        // to this file will actually be stored in the next file.
        // But slice_count is just an FYI and doesn't have to be exact.
        self.slice_count += slice_count;
        for &chunk in chunks {
            let chunk_size = u32::from_be_bytes(raw[chunk..chunk + 4].try_into().unwrap()) as usize;
            if !self.raw.is_empty()
                && self.raw.len() + chunk_size + desc.chunk_head_size > self.flush_after
            {
                self.flush_out(desc)?;
            };
            self.chunks.push(self.raw.len());

            self.raw
                .extend_from_slice(&raw[chunk..chunk + desc.chunk_head_size + chunk_size]);
        }

        Ok(())
    }
}
