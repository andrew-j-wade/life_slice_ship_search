#!/bin/bash

lsss=target/release/life_slice_ship_search

good_column_left=06
good_row_left=0057
good_column_right=08
good_row_right=0058
recovery_column=07
recovery_row=0058

#The parameters below should be from the search being recovered.

data=data

margin=11

#midline=--even_midline
midline=--odd_midline
#midline=--gutter
#midline=--zero # use for Asymmetric, add 1 to margin.

parms="--velocity (2,0)c/7 --rule B3/S23"

# For an exhaustive search re-run the search with seedcolumn changed to all
# the values from 00 to the margin.
# Note however that if a spaceship is found it will not be possible to
# continue the search for that seed as the disk space will blow up.

seedcolumn=00

declare -a exclude

# The exclude parameter can be used to exclude results that are not wanted.
# This could be useful for excluding repeating patterns or lower-period
# spaceships. It is a manual process.

# Usage:
#
# 1. Find the partial that contains the pattern to exclude.
# 2. Pick a slice (two columns) near the middle of the pattern.
# 3. Trim off excess from the tail of the pattern.
# 4. Pass the two columns to --exclude separated by a /. The "left" column
#    goes first.
#
# Notes:
#
# * Trailing spaces in the exclude pattern are significant.
#
# * The results in the partial files are rotated and sometimes flipped. So the
#   left column (that goes first) is the lower row in the odd-numbered
#   partials and the higher row in the even-numbered partials.
#
# * The amount of information to specify what to exclude is fairly limited
#   (Two columns of the pattern for one phase.) To avoid excluding more
#   patterns you do not intend to do not be aggressive in trimming the
#   tail.

#exclude+=(--exclude)
#exclude+=(" **   **    *** ***  ***          *   **               **        ****          **     **        *  *     ****   * /  *    ****   **        *   * *          **  *         *         *  *  * **     *               *  *   *    **** *")


grey_start_row=00
grey_end_row=00
grey_margin=02

flush=4000000000
mem=4000000000
threads=
max_width=100

echo "Preparing left good slices"
for lvl in `seq -w 9999 -2 11`
do
    if [ $lvl -lt $good_row_left ]
    then
        mkdir $data/$lvl
        mkdir $data/$lvl/$good_column_left
        $lsss resize $parms \
          --in_left $data/$prevlvl/$good_column_left \
          --out $data/$lvl/$good_column_left \
          --rows $lvl \
          --sort left || exit 1
        echo $lvl/$good_column_left
   fi
   prevlvl=$lvl
done

echo "Preparing right good slices"
for lvl in `seq -w 9998 -2 11`
do
    if [ $lvl -lt $good_row_right ]
    then
        mkdir $data/$lvl
        mkdir $data/$lvl/$good_column_right
        $lsss resize $parms \
          --in_right $data/$prevlvl/$good_column_right \
          --out $data/$lvl/$good_column_right \
          --rows $lvl \
          --sort right || exit 1
        echo $lvl/$good_column_right
   fi
   prevlvl=$lvl
done

echo "Recovering Slice"
mkdir $data/0010
mkdir $data/0010/$recovery_column

$lsss init $parms --seed ff --slice_rows 10 \
  --sort right --out_dir $data/0010/$recovery_column || exit 1

pass=right
prevlvl=0010
for lvl in `seq -w 11 9999`
do
    if [ $lvl -gt $recovery_row ]
    then
        echo "Recovery Complete"
        exit
    fi

    mkdir $data/$lvl/$recovery_column

    if [ $pass = right ]
    then
        $lsss join $parms $threads \
            --in_left $data/$lvl/$good_column_left \
            --in_right $data/$prevlvl/$recovery_column \
            --out_right $data/$lvl/$recovery_column \
            --right_rows $lvl \
            --right_sort left \
            --flush_right_after $flush \
            --mem $mem || exit 1

        if [ $lvl -lt $good_row_left ]
        then
            rm $data/$lvl/$good_column_left/*.slice
            rmdir $data/$lvl/$good_column_left
        fi
        pass=left
    else
        $lsss join $parms $threads \
            --in_left $data/$prevlvl/$recovery_column \
            --in_right $data/$lvl/$good_column_right \
            --out_left $data/$lvl/$recovery_column \
            --left_rows $lvl \
            --left_sort right \
            --flush_left_after $flush \
            --mem $mem || exit 1

        if [ $lvl -lt $good_row_right ]
        then
            rm $data/$lvl/$good_column_right/*.slice
            rmdir $data/$lvl/$good_column_right
        fi
        pass=right
    fi

    echo $lvl/$recovery_column

    rm $data/$prevlvl/$recovery_column/*.slice
    rmdir $data/$prevlvl/$recovery_column
    if [ $prevlvl -lt $good_row_left -a $prevlvl -lt $good_row_right ]
    then
        rmdir $data/$prevlvl
    fi

    prevlvl=$lvl
done

